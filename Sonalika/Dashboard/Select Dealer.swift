//
//  VehicleTypeView.swift
//  TINFMS
//
//  Created by Vijay on 18/06/18.
//  Copyright © 2018 Vijay. All rights reserved.
//

import UIKit
protocol selectedtype {
    func selectedDealerType(type:String,id:String)
}

class selectDealer: UIView,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tableview:UITableView!
    @IBOutlet weak var str: UILabel!
    
    var type = [String]()
    var ids = [String]()
    var delegate:selectedtype?
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type.count > 0
        {
           return ids.count
        }
        else
        {
            let messageLabel = UILabel(frame:tableview.frame)
            messageLabel.text = "Dealers are not available."
            messageLabel.textAlignment = .center
            tableview.backgroundView = messageLabel
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            tableView.tableFooterView = UIView()
            tableView.frame = CGRect(x: tableView.frame.origin.x, y: tableView.frame.origin.y, width: tableView.frame.size.width, height:tableview.frame.size.height)
            tableView.layer.borderColor = UIColor.lightGray.cgColor
            tableView.layer.cornerRadius = 2.0
            tableView.layer.borderWidth = 1.0
            let cell = UITableViewCell()
            cell.textLabel?.text = ids[indexPath.row]
            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.selectedDealerType(type: type[indexPath.row],id: ids[indexPath.row])
    }

}

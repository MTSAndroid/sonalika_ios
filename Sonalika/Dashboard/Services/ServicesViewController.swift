//
//  ServicesViewController.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class ServicesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SWRevealViewControllerDelegate, Selection, CustomAlertDelegate,sendsmiley,feedback {
   
    
    @IBOutlet weak var servicesListView: UITableView!
    
     var services = NSArray()
     var feedbackView: FeedbackView?
     var smileyView: SmileyView?
    var sendsmiley :SendSmileyView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Services"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        setslideViewController()
        getServices()
     /*   UIView.animate(withDuration: 1.0, delay: 1.0, options: .transitionCrossDissolve, animations: {
            self.smileyView = Bundle.main.loadNibNamed("SmileyView", owner: self, options: nil)?.first as? SmileyView
            self.smileyView?.frame = self.view.bounds
            self.smileyView?.delegate = self
            if let aView = self.smileyView {
                self.view.addSubview(aView)
            }
        }) { finished in
        }*/
       
    }
    
    func getServices() {
        APP_DELEGATE?.showActivityIndicatorOnFullScreen()
        let obj = NetworkManager(utility: networkUtility!)
        var api = ""
        if let aKey = kUserDefaults.value(forKey: KroleCodes)
        {
            if "\(aKey)" == "CLIENT"
            {
                api = "mynavi/servicedone/list?limit=1000"
            }
            else if "\(aKey)" == "DEALER"
            {
                api = "mynavi/servicedone/list?limit=1000?dealerID=\(kUserDefaults.object(forKey: KID)!)"
            }
            else
            {
                api = "mynavi/servicedone/list?limit=1000&customerID=\(kUserDefaults.object(forKey: KID)!)"
            }
        }
        if let aKID = kUserDefaults.value(forKey: KID) {
            obj.getNearsestPetrolStation(url: api)
            {
                (response)in
                if response != nil
                {
                    guard let rows = ((response as? NSDictionary)?.value(forKey: "rows") as? NSArray)else
                    {
                        APP_DELEGATE?.hideActivityIndcicator()
                        return
                    }
                    DispatchQueue.main.async {
                        self.services = rows
                        self.servicesListView.reloadData()
                        APP_DELEGATE?.hideActivityIndcicator()
                    }
                    
                }
            }
        }
    }
    
    func setslideViewController() {
        let revealController: SWRevealViewController? = revealViewController()
        revealController?.delegate = self
        revealController?.rightViewController = nil
        if let aRecognizer = revealController?.panGestureRecognizer {
            view.addGestureRecognizer(aRecognizer())
        }
        let revealButtonItem = UIBarButtonItem(image: UIImage(named: "menu_icon"), style: .plain, target: revealController, action: #selector(revealViewController().revealToggle(_:)))
        navigationItem.leftBarButtonItem = revealButtonItem
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if services.count > 0
        {
           self.servicesListView.backgroundView = nil
           return services.count
        }
        else
        {
            let messageLabel = UILabel(frame: servicesListView.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            servicesListView.backgroundView = messageLabel
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? ServiceViewCell
        cell?.delegate = self
        cell?.viewButton.tag = indexPath.row
        cell?.acknowledgeBtn.tag = indexPath.row
        let dict = services[indexPath.row] as? NSDictionary
        cell?.vehicleNumber.text = dict?["vehicleNumber"] as? String
        cell?.details.text = "\((dict?["deviceModel"] as? String)!)\nDate of Service : \(getDateString(dict?["givenDate"] as? String) ?? "")"
        if let aKey = dict?["servicedone"]
        {
            if "\(aKey)" == "0"
            {
                cell?.viewButton.setTitle("In Progress", for: .normal)
                cell?.viewButton.backgroundColor = UIColor.orange
            }
            else
            {
                cell?.viewButton.setTitle("View", for: .normal)
                if let aKey = dict!["feedbackbycustomer"]
                {
                    if "\(aKey)" == "1"
                    {
                        cell?.acknowledgeBtn.isHidden = true
                    }
                    else
                    {
                        cell?.acknowledgeBtn.isHidden = false
                    }
                }

            }
        }
        return cell!
    }
    
    func getDateString(_ sender: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date: Date? = dateFormatter.date(from: sender ?? "")
        dateFormatter.dateFormat = "dd MMM, yyyy"
        var convertedDateString: String? = nil
        if let aDate = date {
            convertedDateString = dateFormatter.string(from: aDate)
        }
        return convertedDateString
    }
    
    func selectedAction(_ selectedService: Int, forType type: String?) {
        if (type == "View") {
            performSegue(withIdentifier: "Detail", sender: services[selectedService])
        } else {
            let dict = services[selectedService] as? NSDictionary
          /*  UIView.animate(withDuration: 1.0, delay: 1.0, options: .transitionCrossDissolve, animations: {
                self.feedbackView = Bundle.main.loadNibNamed("FeedbackView", owner: self, options: nil)?.first as? FeedbackView
                self.feedbackView?.frame = self.view.bounds
                self.feedbackView?.delegate = self
                self.feedbackView?.iD = dict?["id"] as! String
                if let aView = self.feedbackView {
                    self.view.addSubview(aView)
                }
            }) { finished in
            }*/
            UIView.animate(withDuration: 1.0, delay: 1.0, options: .transitionCrossDissolve, animations: {
                self.sendsmiley = Bundle.main.loadNibNamed("SendSmileyView", owner: self, options: nil)?.first as? SendSmileyView
                self.sendsmiley?.frame = self.view.bounds
                if let aKey = dict!["id"]
                {
                    self.sendsmiley?.id = "\(aKey)"
                }
               
                self.sendsmiley?.feedbackdelegate = self
                if let aView = self.sendsmiley {
                    self.view.addSubview(aView)
                }
            }) { finished in
            }
        }
}
    func selectedActionType(_ type: String?, forId iD: String?, message msg: String?) {
        if (type == "OK") {
            
            let params = ["id": iD ?? "0", "message": msg ?? "0"]
            let obj = NetworkManager(utility: networkUtility!)
            obj.getRequestApiWith(api: "mynavi/servicefeedback", params: params)
            {
                (response,error)in
                if error == nil{
                    self.getServices()
                }
            }
           
            /*HttpManager.apiDetails(["id": iD ?? 0, "message": msg ?? 0], serviceName: "/mynavi/servicefeedback", httpType: kPost, withBlock: { responce, error in
                if error == nil {
                    self.getServices()
                }
            })*/
            feedbackView?.removeFromSuperview()
            feedbackView = nil
        } else {
            feedbackView?.removeFromSuperview()
            feedbackView = nil
        }
    }
    func sendsmileyforservice(status: String) {
        print(status)
    }
    func sendfeedback(result: String,id: String) {

        let params = ["id": id, "message": result]
        let obj = NetworkManager(utility: networkUtility!)
        obj.getRequestApiWith(api: "mynavi/servicefeedback", params: params)
        {
            (response,error)in
            if error == nil{
                print(response)
                self.getServices()
            }
        }
        DispatchQueue.main.async {
           self.sendsmiley?.removeFromSuperview()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Detail") {
            let detailView = segue.destination as? DetailsViewController
            detailView?.details = sender as! NSDictionary
        }
    }
}

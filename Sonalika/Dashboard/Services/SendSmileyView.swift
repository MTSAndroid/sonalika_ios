//
//  SendSmileyView.swift
//  Sonalika
//
//  Created by Manikumar on 09/11/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit

protocol feedback {
    func sendfeedback(result:String,id: String)
}

class SendSmileyView: UIView {

    @IBOutlet weak var exe: UIButton!
    @IBOutlet weak var good: UIButton!
    @IBOutlet weak var avg: UIButton!
    @IBOutlet weak var result: UILabel!
    var id: String?
    var feedbackdelegate: feedback?
    
    @IBAction func avgAction(_ sender: UIButton) {
        switch sender.tag {
        case 10:
            avg.setImage(UIImage(imageLiteralResourceName: "avgcolor"), for: .normal)
                avg.tag = 11
            good.setImage(UIImage(imageLiteralResourceName: "emojigraygood76"), for: .normal)
            good.tag = 12
            exe.setImage(UIImage(imageLiteralResourceName: "emojigrayexe76"), for: .normal)
            exe.tag = 14
            self.result.text = "Bad"
            break
        case 11:
            avg.setImage(UIImage(imageLiteralResourceName: "emojigray76"), for: .normal)
            avg.tag = 10
            good.setImage(UIImage(imageLiteralResourceName: "emojigraygood76"), for: .normal)
            good.tag = 12
            exe.setImage(UIImage(imageLiteralResourceName: "emojigrayexe76"), for: .normal)
            exe.tag = 14
            self.result.text = ""
            break
        default:
            break
        }
        
    }
    
    @IBAction func goodAction(_ sender: UIButton) {

        switch sender.tag {
        case 12:
            good.setImage(UIImage(imageLiteralResourceName: "goodcolor"), for: .normal)
            good.tag = 13
            avg.setImage(UIImage(imageLiteralResourceName: "emojigray76"), for: .normal)
            avg.tag = 10
            exe.setImage(UIImage(imageLiteralResourceName: "emojigrayexe76"), for: .normal)
            exe.tag = 14
            self.result.text = "Good"
            break
        case 13:
            good.setImage(UIImage(imageLiteralResourceName: "emojigraygood76"), for: .normal)
            good.tag = 12
            avg.setImage(UIImage(imageLiteralResourceName: "emojigray76"), for: .normal)
            avg.tag = 10
            exe.setImage(UIImage(imageLiteralResourceName: "emojigrayexe76"), for: .normal)
            exe.tag = 14
            self.result.text = ""
            break
        default:
            break
        }
    }
    
    @IBAction func exeAction(_ sender: UIButton) {
        switch sender.tag {
        case 14:
            exe.setImage(UIImage(imageLiteralResourceName: "execolor"), for: .normal)
            exe.tag = 15
            avg.setImage(UIImage(imageLiteralResourceName: "emojigray76"), for: .normal)
            avg.tag = 10
            good.setImage(UIImage(imageLiteralResourceName: "emojigraygood76"), for: .normal)
            good.tag = 12
            self.result.text = "Excellent"
            break
        case 15:
            exe.setImage(UIImage(imageLiteralResourceName: "emojigrayexe76"), for: .normal)
            exe.tag = 14
            avg.setImage(UIImage(imageLiteralResourceName: "emojigray76"), for: .normal)
            avg.tag = 10
            good.setImage(UIImage(imageLiteralResourceName: "emojigraygood76"), for: .normal)
            good.tag = 12
            self.result.text = ""
            break
        default:
            break
        }
    }
    
    @IBAction func smileyAction(_ sender: Any) {
        if result.text == ""
        {
//            let alertController = UIAlertController(title: "Feedback", message: "Please send your valuable feedback", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
//            alertController.addAction(ok)
            let alert = UIAlertView()
            alert.title = "Feedback"
            alert.message = "Please send your valuable feedback"
            alert.addButton(withTitle: "ok")
            alert.show()
        }
        else
        {
            self.feedbackdelegate?.sendfeedback(result: result.text!, id: id!)
        }
    }
}

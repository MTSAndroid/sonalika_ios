//
//  SmileyView.swift
//  Sonalika
//
//  Created by Manikumar on 08/11/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit
protocol sendsmiley {
    func sendsmileyforservice(status:String)
}

class SmileyView: UIView {
    
    
    var delegate: sendsmiley?
    @IBOutlet weak var smileystatus: UILabel!
    
    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var smileyImg: UIImageView!
    override func awakeFromNib() {
      
    }

    @IBAction func sliderValueChanged(_ sender: UISlider) {
        
        let currentValue = Int(sender.value)
        switch currentValue {
        case 0:
            smileystatus.text = "Below Average"
            smileyImg.image = UIImage(imageLiteralResourceName: "belowAverage")
            break
        case 25:
            smileystatus.text = "Average"
            smileyImg.image = UIImage(imageLiteralResourceName: "Average")
            break
        case 50:
            smileystatus.text = "Good"
            smileyImg.image = UIImage(imageLiteralResourceName: "Excellent")
            break
        case 75:
            smileystatus.text = "Very Good"
            smileyImg.image = UIImage(imageLiteralResourceName: "belowAverage")
            break
        case 100:
            smileystatus.text = "Excellent"
            smileyImg.image = UIImage(imageLiteralResourceName: "Excellent")
            break
        default:
            break
        }
    }
    @IBAction func sendsmiley(_ sender: UIButton) {
        delegate?.sendsmileyforservice(status: smileystatus.text!)
    }
}

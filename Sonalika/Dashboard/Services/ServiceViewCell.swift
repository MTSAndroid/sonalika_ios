//
//  ServiceViewCell.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

protocol Selection: class {
    func selectedAction(_ selectedService: Int, forType type: String?)
}

class ServiceViewCell: UITableViewCell {
    @IBOutlet weak var vehicleNumber: UILabel!
    @IBOutlet weak var details: UILabel!
    @IBOutlet weak var viewButton: UIButton!
    @IBOutlet weak var acknowledgeBtn: UIButton!
    weak var delegate: Selection?
    
    @IBAction func viewService(_ sender: UIButton) {
        if (sender.currentTitle == "View") {
            delegate?.selectedAction(sender.tag, forType: "View")
        }
    }
    
    @IBAction func acknowledge(_ sender: UIButton) {
        delegate?.selectedAction(sender.tag, forType: "Acknowledge")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewButton.layer.borderWidth = 0.5
        viewButton.layer.borderColor = UIColor.white.cgColor
        acknowledgeBtn.layer.borderWidth = 0.5
        acknowledgeBtn.layer.borderColor = UIColor.white.cgColor
        acknowledgeBtn.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

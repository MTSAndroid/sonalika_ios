//
//  VehiclesView.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

protocol SelectionDelegate: class {
    func selectedVehicleNumber(_ number: String?)
    
    func removeView()
}

class VehiclesView: UIView, UITableViewDelegate, UITableViewDataSource {
    var vehicles:NSArray?
    weak var delegate: SelectionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicles!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        }
        let obj = vehicles![indexPath.row] as? VehicleObject
        cell?.textLabel?.text = obj?.vehicleNumber.uppercased()
        cell?.textLabel?.textAlignment = .center
        if let aCell = cell {
            return aCell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: UITableViewCell? = tableView.cellForRow(at: indexPath)
        delegate?.selectedVehicleNumber(cell?.textLabel?.text)
    }
    
    @IBAction func remove(_ sender: Any) {
        delegate?.removeView()
    }
}

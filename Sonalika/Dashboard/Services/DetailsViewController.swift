//
//  DetailsViewController.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var details:NSDictionary!
    @IBOutlet var detailView: UITableView!
    
    private var requiredDetails = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        title = details["vehicleNumber"] as? String
        navigationItem.leftBarButtonItems = CustomControls.sharedObj.leftBarButtonMethod("arrow_left", selector: #selector(DetailsViewController.popAction(_:)), delegate: self) as? [UIBarButtonItem]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        detailView.rowHeight = UITableViewAutomaticDimension
        detailView.estimatedRowHeight = 150.0
        detailView.tableFooterView = UIView()
        requiredDetails["Service Date"] = "\(getDateString(details["givenDate"] as? String)!)"
        
        if details["maintanancedetails"] != nil {
            requiredDetails["Maintenance Details"] = "\(details["maintanancedetails"]!)"
            
        }
        if details["rectifiedDate"] != nil {
            requiredDetails["Rectified Date"] = "\(getDateString(details["rectifiedDate"] as? String) ?? ""))"
        }
        requiredDetails["Service No"] = "\(details["serviceNumber"]!)"
        
        requiredDetails["service schedule hours"] = "\(details["actualHours"]!)"
        
         requiredDetails["Variant Code"] = "\(details["varient"]!)"
       
        requiredDetails["Dealer Location"] = "\(details["dealerlocation"]!)"
         requiredDetails["Dealer Code"] = "\(details["dealercode"]!)"
    
        requiredDetails["Actual hours"] = "\(details["engineHours"]!)"
        
         requiredDetails["Service Type"] = "\(details["serviceType"]!)"
        if details["machinefaults"] != nil {
            if "\(details["machinefaults"]!)".contains(",") {

                requiredDetails["Machine Faults"] = "\((((details.value(forKey: "machinefaults") as? String))?.replacingOccurrences(of: ",", with: "\n"))!)"
            } else {
                
                requiredDetails["Machine Faults"] = "\(details["machinefaults"]!)"
            }
        }
        if details["cost"] != nil {
        
            requiredDetails["cost"] = "\(details["cost"]!)"
        }
        if details["enginefaults"] != nil {
            if "\(details["enginefaults"]!)".contains(",") {
            
                requiredDetails["Engine Faults"] = "\((((details.value(forKey: "enginefaults") as? String))?.replacingOccurrences(of: ",", with: "\n"))!)"
            } else {
                requiredDetails["Engine Faults"] = "\(details["enginefaults"]!)"
            }
        }
        if details["varienceinHours"] != nil {
          
            requiredDetails["Variance in terms of hours "] = "\(details["varienceinHours"]!)"
        }
    
        self.detailView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (requiredDetails.count) > 0
        {
            return (requiredDetails.count)
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CustomViewCell
        let keys = Array(requiredDetails.keys)
        let values = Array(requiredDetails.values)
        cell?.name.text = keys[indexPath.row]
        if values[indexPath.row] != "<null>" || values[indexPath.row] != ""
        {
            cell?.value.text = values[indexPath.row]
        }
        else
        {
            cell?.value.text = "NA"
        }
        return cell!
    }
    
    func getDateString(_ sender: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date: Date? = dateFormatter.date(from: sender ?? "")
        dateFormatter.dateFormat = "dd MMM, yyyy"
        var convertedDateString: String? = nil
        if let aDate = date {
            convertedDateString = dateFormatter.string(from: aDate)
        }
        return convertedDateString
    }
    
    @objc func popAction(_ sender: Any?) {
        navigationController?.popViewController(animated: true)
    }
}

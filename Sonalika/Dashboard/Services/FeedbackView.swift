//
//  FeedbackView.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit
import SAMTextView



protocol CustomAlertDelegate: class {
    func selectedActionType(_ type: String?, forId iD: String?, message msg: String?)
}

class FeedbackView: UIView, UITextViewDelegate {
    var iD = ""
    @IBOutlet weak var textView: SAMTextView!
    weak var delegate: CustomAlertDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.layer.borderWidth = 0.5
        textView.placeholder = "Please enter your feedback"
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let resultRange: NSRange = (text as NSString).rangeOfCharacter(from: CharacterSet.newlines, options: .backwards)
        if text.count == 1 && Int(resultRange.location) != NSNotFound {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func okAction(_ sender: Any) {
        delegate?.selectedActionType("OK", forId: iD, message: textView.text)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        delegate?.selectedActionType("Cancel", forId: iD, message: textView.text)
    }
}

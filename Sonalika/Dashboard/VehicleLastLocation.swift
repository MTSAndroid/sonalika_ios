//
//  VehicleLastLocation.swift
//  Sonalika
//
//  Created by Manikumar on 06/11/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit
import GoogleMaps

class VehicleLastLocation: UIViewController,GMSMapViewDelegate {

    @IBOutlet var mapview: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapview.delegate = self
        self.navigationItem.backBarButtonItem?.image = UIImage(imageLiteralResourceName: "arrow_left")
        
        self.view.layoutIfNeeded()
        addGoogleMapView()
        drawPins()

    }
    func addGoogleMapView() {
        
        let camera = GMSCameraPosition.camera(withLatitude: (Double(TabBarSingleTon.sharedObj.vlat))!, longitude: (Double(TabBarSingleTon.sharedObj.vlng))!, zoom: 15.0)
        self.mapview.camera = camera
        var frame: CGRect = view.frame
        frame.origin.y = 64
        frame.size.height = view.frame.size.height - 156
        mapview?.settings.compassButton = true
    }
    func drawPins() {
        var bounds = GMSCoordinateBounds()
        let marker = GMSMarker()
        let latitude = (Double(TabBarSingleTon.sharedObj.vlat))!
        let longitude = (Double(TabBarSingleTon.sharedObj.vlng))!
        marker.position = CLLocationCoordinate2DMake(latitude , longitude )
        bounds = bounds.includingCoordinate(marker.position)
        let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        self.getAddressFromGeocodeCoordinate(coordinate: coordinates)
        {
            (response,error) in
            if error == nil
            {
                guard let result = response    else
                {
                    return
                }
                var title = (((result as? NSArray)?.firstObject as! String))
                title = title.replacingOccurrences(of: " ", with: "")
                marker.snippet = title.replacingOccurrences(of: ",", with: "\n")
            }
        }
      //  marker.title = "\(latitude)" + " " + "\(longitude)"
        marker.map = self.mapview
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        return true
    }
    
    
    func getAddressFromGeocodeCoordinate(coordinate: CLLocationCoordinate2D,completion: @escaping (Any?,Error?) -> Void) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
        
            if error == nil
            {
                if let address = response!.firstResult() {
                    let lines = address.lines! as [String]
                    
                    completion(lines,nil)
                }
            }
            else
            {
                completion(nil,error)
            }
        }
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

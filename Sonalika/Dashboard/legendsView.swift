//
//  legendsView.swift
//  TINFMS
//
//  Created by Vijay on 22/06/18.
//  Copyright © 2018 Vijay. All rights reserved.
//


import UIKit

protocol close {
    func closeview()
}

class legendsView: UIView {
    
    var delegate:close?
    @IBOutlet weak var speedLbl: UILabel!
    
    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var mapLbl: UILabel!
    @IBAction func closeBtn(_ sender: UITapGestureRecognizer) {
       self.delegate?.closeview()
    }
}

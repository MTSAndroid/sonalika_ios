
//
//  DashboardViewController.swift
//  Sonalika
//
//  Created by Manikumar on 25/10/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController,selectedtype,DatePickerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,close{
    @IBOutlet weak var toDateView: UIView!
    @IBOutlet weak var submitview: UIView!
    @IBOutlet weak var fromDateView: UIView!
    @IBOutlet weak var fromdateLbl: UILabel!
    @IBOutlet weak var todateLbl: UILabel!
    @IBOutlet weak var dealerView: UIView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var dealerLbl: UILabel!
    @IBOutlet weak var legends: UIView!
    
    @IBOutlet weak var customertop: NSLayoutConstraint!
    @IBOutlet weak var customerView: UIView!
    var selectDealerView: selectDealer!
    var datePickerview:DatePickerView!
    var legendview: legendsView!
    var istodatetapped:Bool = false
    var isfromdatetapped:Bool = false
    var ispickerviewadded = false
    var isdealerselected = false
    var islegend = false
    var tdate = Date()
    var fdate,todate:String?
    var dealerarr = NSArray()
    var dealerdetailarr = NSArray()
    var id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Track Vehicles"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if revealViewController() != nil
        {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        self.dealerView.layer.cornerRadius = 5.0
        self.dealerView.layer.borderColor = UIColor.black.cgColor
        self.dealerView.layer.borderWidth = 1.0
        self.fromDateView.layer.cornerRadius = 5.0
        self.fromDateView.layer.borderWidth = 1.0
        self.fromDateView.layer.borderColor = UIColor.black.cgColor
        self.toDateView.layer.cornerRadius  = 5.0
        self.toDateView.layer.borderWidth = 1.0
        self.toDateView.layer.borderColor = UIColor.black.cgColor
        self.submitview.layer.cornerRadius = 3.0
   //     self.todateLbl.text = CustomControls.sharedObj.getCurrentDate()
        let dateFormatter = DateFormatter()
   
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let threedays = (Calendar.current.date(byAdding: .day, value: -4, to: Date()))
        let convertedDateString = dateFormatter.string(from: threedays!)
      //  self.fromdateLbl.text = convertedDateString
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "MMM dd, yyyy"
        self.fromdateLbl.text = formatter.string(from: threedays!)
        self.todateLbl.text = formatter.string(from: Date())
        
        self.fdate = controls.beginningOfDay(threedays!)
        self.todate = controls.endOfDay(tdate)
        self.view.addActivity()
        self.togetDealers()
        self.togetengineHours()
      //  self.tableview.allowsSelection = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isdealerselected = false
    }

    @IBAction func selectDealer(_ sender: UITapGestureRecognizer) {

       if self.isdealerselected == false
        {
            self.isdealerselected = true
        UIView.animate(withDuration: 0.25, animations: {
            self.selectDealerView = Bundle.main.loadNibNamed("SelectDealer", owner: self, options: nil)?.first as! selectDealer
            if let aKey = kUserDefaults.value(forKey: KroleCodes)
            {
                if "\(aKey)" == "CLIENT"
                {
                    self.selectDealerView.type.append("Select Dealer")
                    self.selectDealerView.ids.append("Select Dealer")
                    self.selectDealerView.str.text = "Select Dealer"
                }
                else
                {
                    self.selectDealerView.type.append("Select Customer")
                    self.selectDealerView.ids.append("Select Customer")
                    self.selectDealerView.str.text = "Select Customer"
                }
                
            }
            for dict in self.dealerarr
            {
                let dict1 = dict as? NSDictionary
                let extras = (dict1)?.object(forKey: "extras") as? NSDictionary
                self.selectDealerView.type.append((extras?.object(forKey: "dealerID") as? String)!)
                self.selectDealerView.ids.append((dict1?.object(forKey: "id") as? String)!)
            }
            
            let Ypos = (self.dealerView.frame.origin.y + self.dealerView.frame.size.height + 10)
            let Xpos = (self.dealerView.frame.origin.x)
            var height = (self.view.bounds.size.height / 2)
            
            self.selectDealerView.frame = CGRect(x: Xpos, y: 124.0, width: self.dealerView.frame.size.width, height: height)
            
            self.selectDealerView.delegate = self
        
            self.view.addSubview(self.selectDealerView)
            self.view.bringSubview(toFront: self.selectDealerView)
        
        })
        }
       /* let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let dealertype = storyboard.instantiateViewController(withIdentifier: "dealertype") as? DealertypeViewcontroller
        for dict in self.dealerarr
        {
            let dict1 = dict as? NSDictionary
            let extras = (dict1)?.object(forKey: "extras") as? NSDictionary
            dealertype?.type.append((extras?.object(forKey: "dealerID") as? String)!)
            dealertype?.ids.append((dict1?.object(forKey: "id") as? String)!)
        }
        self.navigationController?.pushViewController(dealertype!, animated: true)*/
        
    }
    func selectedDealerType(type: String,id: String) {
        DispatchQueue.main.async {
            self.dealerLbl.text = id
            self.isdealerselected = false
            self.id = id
            self.selectDealerView.removeFromSuperview()
            self.togetengineHours()
        }
    }
    func togetDealers()
        
    {
        let obj = NetworkManager(utility: networkUtility!)
        var api = ""
        if let aKey = kUserDefaults.value(forKey: KroleCodes)
        {
            if "\(aKey)" == "CLIENT"
            {
               self.dealerLbl.text = "Select Dealer"
                api = "auth/user/edit?active=true&extras.clientID=\(kUserDefaults.value(forKey: KID)!)&limit=100&page=1&roleCodes=DEALER"
            }
            else if "\(aKey)" == "DEALER"
            {
                self.dealerLbl.text = "Select Customer"
                api = "auth/user/edit?active=true&extras.dealerID=\(kUserDefaults.value(forKey: KID)!)&limit=100&page=1&roleCodes=CUSTOMER"
            }
            else
            {
                self.dealerView.isHidden = true
                self.customertop.constant = 12.0
            }
        }
        if api != ""
        {
        obj.getNearsestPetrolStation(url: api)
        {
            (response) in
            guard let response = response as? NSDictionary else
            {
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
                 return
            }
         
            DispatchQueue.main.async {
                let rows = response.object(forKey: "rows") as? NSArray
                self.dealerarr = rows!
                self.view.removeActivity()
            }
        }
        }
    }
    @IBAction func legendsview(_ sender: UITapGestureRecognizer) {
        if islegend == false
        {
        self.islegend = true
        UIView.animate(withDuration: 0.25, animations: {
            self.legendview = Bundle.main.loadNibNamed("legendview", owner: self, options: nil)?.first as! legendsView
            self.legendview.layer.borderColor = UIColor.black.cgColor
            self.legendview.layer.borderWidth = 1.0
            self.legendview.layer.cornerRadius = 1.0
            let Ypos = (self.customerView.frame.origin.y + self.legends.frame.size.height + self.legends.frame.origin.y + 10)
            let Xpos = (self.legends.frame.origin.x)
            self.legendview.frame = CGRect(x: Xpos, y: Ypos, width: self.legends.frame.size.width, height:125)
            self.legendview.delegate = self
            self.view.addSubview(self.legendview)
            
        })
        }
    }
    func closeview() {
        DispatchQueue.main.async {
            self.legendview.removeFromSuperview()
            self.islegend = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func fromDate(_ sender: UITapGestureRecognizer) {
        self.addDatePickerview(tag: (sender.view?.tag)!)
    }
    
    func addDatePickerview(tag:Int)
    {
        if ispickerviewadded == false
        {
            self.ispickerviewadded = true
            datePickerview = Bundle.main.loadNibNamed(kDatapicker, owner: self, options: nil)?.first as! DatePickerView
            let Ypos = (fromDateView.frame.origin.y + fromDateView.frame.size.height + 5)
            datePickerview.frame = CGRect(x: 0, y: Ypos, width: self.view.frame.width, height:self.view.frame.size.height - Ypos)
            datePickerview.datePicker.datePickerMode = .dateAndTime
            datePickerview.datePicker.maximumDate = Date()
            datePickerview.delegate = self as DatePickerDelegate
            //  datePickerview.doneBtn.isHidden = true
            
            if tag == 10{
                isfromdatetapped = true
                istodatetapped = false
                datePickerview.datePicker.minimumDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
            }
            else{
                istodatetapped = true
                isfromdatetapped = false
                if tdate != Date(){
                    datePickerview.datePicker.minimumDate = tdate
                }
            }
            self.view.addSubview(datePickerview)
        }
    }
    func doneClicked(sender: Date) {
        let date = sender
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "MMM dd, yyyy"
        if isfromdatetapped
        {
            fromdateLbl.text = formatter.string(from: date)
            fdate = controls.beginningOfDay(date)
        }
        else
        {
            if tdate != nil
            {
                datePickerview.datePicker.minimumDate = tdate
                todateLbl.text = formatter.string(from: date)
                todate = controls.endOfDay(date)!
            }
        }
        //        let dateformatter = DateFormatter()
        //        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //        let todate = dateformatter.date(from: (toDate.text)!)
        //        let fromdate = dateformatter.date(from: (fromDate.text)!)
        //        print(todate)
        //        print(fromdate)
        //        if todate! < fromdate!
        //        {
        //          let alert = UIAlertView(title: "Alert", message: "To date should greather than from date", delegate: self, cancelButtonTitle: "ok")
        //          alert.show()
        //        }
        self.removeDatePickerview()
    }
    
    func selectedDate(sender: Date) {
        let date = sender
        
        if isfromdatetapped
        {
            tdate = date
            fromdateLbl.text = "\(date)"
        }
        else
        {
            todateLbl.text = "\(date)"
        }
    }
    func removeDatePickerview()
    {
        self.datePickerview?.removeFromSuperview()
        self.datePickerview.datePicker = nil
        isfromdatetapped = false
        istodatetapped = false
        ispickerviewadded = false
    }
    
    @IBAction func submitAction(_ sender: UITapGestureRecognizer) {

        togetengineHours()
    }
    func togetengineHours()
    {
        self.view.addActivity()
        var objdict = [String:NSDictionary]()
        objdict = ["lastEngineOn":["lte":todate!,"gte":fdate!]]
        var str = ""
        do {
            let data = try JSONSerialization.data(withJSONObject: objdict, options: .prettyPrinted)
            str =  String(data: data, encoding: String.Encoding.utf8) ?? ""
        } catch {
            str = "Not stringified"
            print("Not Stringified")
        }
        var api = ""
        if let aKey = kUserDefaults.value(forKey: KroleCodes)
        {
            if "\(aKey)" == "DEALER"
            {
                if id == "" || id == "Select Customer"
                {
                  id = "\(kUserDefaults.value(forKey: KID)!)"
                }
                api = "mynavi/dvmap/list?dealerID=\(id)&limit=100&page=1&range=\(str)"
            }
            else if "\(aKey)" == "CUSTOMER"
            {
                
                id = "\(kUserDefaults.value(forKey: KID)!)"
                api = "mynavi/dvmap/list?customerID=\(id)&limit=100&page=1&range=\(str)"
            }
            else
            {
                if id == "" || (self.dealerLbl.text == "Select Dealer")
                {
                    api = "mynavi/dvmap/list?limit=100&page=1&range=\(str)"
                }
                else
                {
                    api =  "mynavi/dvmap/list?dealerID=\(id)?limit=100&page=1&range=\(str)"
                }
            }
        }
//        if self.dealerLbl.text == "Select Dealer"
//        {
//            api = "mynavi/dvmap/list?limit=100&page=1&range=\(str)"
//        }
//        else
//        {
//              api = "mynavi/dvmap/list?dealerID=\(id)&limit=100&page=1&range=\(str)"
//        }
        let obj = NetworkManager(utility: networkUtility!)
        self.view.addActivity()
        obj.getNearsestPetrolStation(url: api)
        {
            (response) in
            guard let response = response as? NSDictionary else
            {
                DispatchQueue.main.async {
                    self.view.removeActivity()
                }
                return
            }
            let rows = response.object(forKey: "rows") as? NSArray
            self.dealerdetailarr = rows!
            DispatchQueue.main.async {
                self.collectionview.reloadData()
                self.view.removeActivity()
            }
        }
    }
 /*   func numberOfSections(in tableView: UITableView) -> Int {
       
        if self.dealerdetailarr.count > 0 {
            tableview.backgroundView = nil
            return dealerdetailarr.count
        } else {
            let messageLabel = UILabel(frame:tableview.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            tableview.backgroundView = messageLabel
        }
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? dealertableCell
        let dict = dealerdetailarr[indexPath.section] as? NSDictionary
        cell?.sno.text = "\(indexPath.section + 1)"
        cell?.BOM.text = dict?.object(forKey: "varient") as? String
        cell?.chassisNo.text = dict?.object(forKey: "pin") as? String
        cell?.distancetrl.text = "\((dict?.object(forKey: "distance") as? Double ?? 0.0))"
        cell?.lastlocation.text = dict?.object(forKey: "totalEngineHours") as? String
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row % 5 == 0
        {
            print("chassis no is selected")
        }
        else
        {
            print("selection is not allowed")
        }
    }*/
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 0, 5, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = (UI_USER_INTERFACE_IDIOM() == .pad) ? 64 : 44
    
        return CGSize(width: (collectionView.frame.width / 7 - 10), height: height)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if self.dealerdetailarr.count > 0 {
            collectionview.backgroundView = nil
            return dealerdetailarr.count
        } else {
            let messageLabel = UILabel(frame:collectionview.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            collectionview.backgroundView = messageLabel
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! dealerCollectionViewCell
//        cell.layer.borderColor = (UIColor.black.cgColor)
//        cell.layer.borderWidth = 0.5
        let dict = dealerdetailarr[indexPath.section] as? NSDictionary
        if indexPath.row == 0
        {
            cell.dataLbl.text = "\(indexPath.section + 1)"
        }
        else if indexPath.row == 1
        {
            cell.dataLbl.text = dict?.object(forKey: "varient") as? String
        }
        else if indexPath.row == 2
        {
            cell.dataLbl.text = dict?.object(forKey: "pin") as? String
        }
        else if indexPath.row == 3
        {
            if let aKey = dict!["totalEngineHours"]
            {
                if "\(aKey)" != ""
                {
                    cell.dataLbl.text = "\(aKey)"
                }
                else
                {
                    cell.dataLbl.text = "00:00:00"
                }
            }
        }
            else if indexPath.row == 4
        {
            if let aKey = dict!["lastEngineOn"]
            {
                if "\(aKey)" != ""
                {
                    let akey = ("\(aKey)").replacingOccurrences(of: "T", with: " ")
                    cell.dataLbl.text = akey.replacingOccurrences(of: ".000Z", with: "")
                }
                else
                {
                    cell.dataLbl.text = "NA"
                }
            }
        }
            else if indexPath.row == 5
        {
            let attributedstr = NSMutableAttributedString(string: "")
            let attachment = NSTextAttachment()
            attachment.image = UIImage(imageLiteralResourceName: "location")
            
            attributedstr.append(NSAttributedString(attachment: attachment))
            var latlng = ""
            if let aKey = dict!["lat"]
            {
                if "\(aKey)" != ""
                {
                    latlng = "\(aKey)"
                }
                else
                {
                    latlng = "NA"
                }
            }
            if let aKey = dict!["lng"]
            {
                if "\(aKey)" != ""
                {
                    latlng += " " + "\(aKey)"
                }
                else
                {
                    latlng = "NA"
                }
            }
            //cell.dataLbl.text = latlng
            cell.dataLbl.attributedText = attributedstr
        }
        else if indexPath.row == 6
        {
            if let aKey = dict!["distance"]
             {
               if "\(aKey)" != ""
               {
                 cell.dataLbl.text = ("\(aKey)").trunc(4)
                }
                else
               {
                 cell.dataLbl.text = "0.0"
                }
             }
            
        }
        if indexPath.row == 2 || indexPath.row == 5
        {
            cell.dataLbl.textColor = UIColor(red:19/255 , green: 69/255, blue: 252/255, alpha: 1.0)
        }
        else
        {
            cell.dataLbl.textColor = UIColor.black
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let dict = dealerdetailarr[indexPath.section] as? NSDictionary
         let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        if indexPath.row == 2
        {
           
          
            TabBarSingleTon.sharedObj.deviceID = dict?.value(forKey: "deviceID") as! String
            TabBarSingleTon.sharedObj.vehicleDict = dict!
           
         
            let tabViewControllerObj = storyBoard.instantiateViewController(withIdentifier: "TabViewController") as? TabViewController
            
          //  tabViewControllerObj?.vehicleObject = vehicleListArray[indexPath.row] as? VehicleObject
            self.navigationController?.pushViewController(tabViewControllerObj!, animated: true)
        }
        if indexPath.row == 5
        {
            
            TabBarSingleTon.sharedObj.vlat = (dict?.object(forKey: "lat") as? String)!
            TabBarSingleTon.sharedObj.vlng = (dict?.object(forKey: "lng") as? String)!
            let obj = storyBoard.instantiateViewController(withIdentifier: "VLL") as? VehicleLastLocation
            self.navigationController?.pushViewController(obj!, animated: true)
            
        }
    }
}

extension Date
{
    func days(from date: Date, to todate: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: todate).day ?? 0
    }
}
extension String {
    
    func trunc(_ length: Int) -> String {
        if self.contains(".")
        {
            let strindex =  self.index(of: ".")
            return self.substring(to: self.characters.index(strindex!, offsetBy: length))
        }
        else
        {
            return self
        }
    }
    
    func trim() -> String{
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
}
@IBDesignable
class DateView:UIView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBInspectable
    public var borderColor:UIColor = .white{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable
    public var borderWidth:CGFloat = 0.5{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    @IBInspectable
    public var cornerRadius:CGFloat = 0.5
    {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
}
class labelBorder:UILabel
{
    @IBInspectable
    public var borderColor:UIColor = .white{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable
    public var borderWidth:CGFloat = 0.5{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
}
class buttonBorder: UIButton {
    @IBInspectable
    public var cornorRadius:CGFloat = 0.5
    {
        didSet
        {
            self.layer.cornerRadius = cornorRadius
        }
    }
    @IBInspectable
    public var borderColor:UIColor = .white{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable
    public var borderWidth:CGFloat = 0.5{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
}
class Imageborder: UIImageView
{
    @IBInspectable
    public var cornerRadius: CGFloat = 0.5
    {
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    @IBInspectable
    public var borderColor:UIColor = .white{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable
    public var borderWidth:CGFloat = 0.5{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
}


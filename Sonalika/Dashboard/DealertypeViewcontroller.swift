//
//  DealertypeViewcontroller.swift
//  Sonalika
//
//  Created by Manikumar on 01/11/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit
protocol selectdealer {
    func selecteddealertype(type:String,id:String)
}


class DealertypeViewcontroller: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    var type = [String]()
    var ids = [String]()
    @IBOutlet weak var tableview:UITableView!
    var delegate:selectdealer?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if type.count > 0
        {
            return type.count
        }
        else
        {
            let messageLabel = UILabel(frame:tableview.frame)
            messageLabel.text = "Dealers are not available."
            messageLabel.textAlignment = .center
            tableview.backgroundView = messageLabel
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        return cell
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

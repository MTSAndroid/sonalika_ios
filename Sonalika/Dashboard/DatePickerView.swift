//
//  DatePickerView.swift
//  TINFMS
//
//  Created by Vijay on 14/06/18.
//  Copyright © 2018 Vijay. All rights reserved.
//

import UIKit

protocol DatePickerDelegate {
    func doneClicked(sender:Date)
    func selectedDate(sender:Date)
    
}

class DatePickerView: UIView {
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var doneBtn:UIButton!
    var delegate:DatePickerDelegate?
    var date:Date?
    
    override func awakeFromNib() {

    }
    @IBAction func done(_ sender: Any) {
        if date != nil
        {
            self.delegate?.doneClicked(sender: date!)
        }
        else
        {
          self.delegate?.doneClicked(sender: Date())
        }
        
    }
    @IBAction func datePickerAction(_ sender: UIDatePicker) {
        date = sender.date
       // self.delegate?.selectedDate(sender: sender.date)
    }
    
}



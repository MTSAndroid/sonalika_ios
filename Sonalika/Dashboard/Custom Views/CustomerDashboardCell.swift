//
//  CustomerDashboardCell.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class CustomerDashboardCell: UITableViewCell {
    @IBOutlet weak var model: UILabel!
    @IBOutlet weak var vehicleNumber: UILabel!
    @IBOutlet weak var nextService: UILabel!
    @IBOutlet weak var engineHours: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}

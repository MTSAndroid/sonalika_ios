//
//  PaddingLabel.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class PaddingLabel: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        let insets = [0, 10, 0, 10] as? UIEdgeInsets
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets!))
    }
}

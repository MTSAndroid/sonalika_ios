//
//  DatesView.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class DatesView: UIView {
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var fromDate: UIView!
    @IBOutlet weak var toDate: UIView!
    
    func loadData() {
        fromDate.layer.cornerRadius = 5.0
        fromDate.layer.masksToBounds = true
        fromDate.layer.borderColor = UIColor.gray.cgColor
        fromDate.layer.borderWidth = 1.0
        toDate.layer.cornerRadius = 5.0
        toDate.layer.masksToBounds = true
        toDate.layer.borderColor = UIColor.gray.cgColor
        toDate.layer.borderWidth = 1.0
    }
}

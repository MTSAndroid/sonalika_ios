//
//  CustomPickerView.swift
//  Ajax fiori Swift
//
//  Created by Vijay on 08/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

 protocol PickerViewProtocal: NSObjectProtocol {
    func pickerViewSelectedValue(_ sender: String?, forRow row: Int)
}

class CustomPickerView: UIView,UIPickerViewDelegate,UIPickerViewDataSource {
    var categoryPickerView: UIPickerView?
    var pickerSelectedRow: Int = 0
    var dataArray = [Any]()
    weak var delegate: PickerViewProtocal?
    
    var pickerSelectedString = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        baseInit(frame)
    }
    
    func baseInit(_ frame: CGRect) {
        print(dataArray)
        backgroundColor = UIColor(red: 240 / 255.0, green: 240 / 255.0, blue: 240 / 255.0, alpha: 1.0)
        categoryPickerView = UIPickerView(frame: CGRect(x: 0, y: 64, width: self.frame.size.width, height: self.frame.size.height-64))
       // categoryPickerView?.backgroundColor = UIColor.groupTableViewBackground
        categoryPickerView?.backgroundColor = backgroundColor
        categoryPickerView?.delegate = self
    
        let pickerToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 64))
        pickerToolbar.barStyle = .blackOpaque
        pickerToolbar.barTintColor = UIColor.red
        pickerToolbar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(CustomPickerView.categoryDoneButtonPressed))
        doneBtn.tintColor = UIColor.white
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(CustomPickerView.categoryCancelButtonPressed))
        cancelBtn.tintColor = UIColor.white
        pickerToolbar.setItems([cancelBtn, flexSpace, doneBtn], animated: true)
        self.addSubview(pickerToolbar)
        self.addSubview(categoryPickerView!)
}
    func setSelectedValue(_ row: Int) {
        categoryPickerView?.selectRow(row, inComponent: 0, animated: true)
    }
    
    @objc func categoryDoneButtonPressed() {
     
        delegate?.pickerViewSelectedValue(pickerSelectedString, forRow: pickerSelectedRow)
        removeFromSuperview()
    }
    
    @objc func categoryCancelButtonPressed() {
        removeFromSuperview()
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if dataArray.count != 0 {
            pickerSelectedString = dataArray[0] as? String ?? ""
        }
        return dataArray.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataArray[row] as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //    if(row)
        //    {
        pickerSelectedString = dataArray[row] as? String ?? ""
        pickerSelectedRow = row
        //    }
        //    else
        //    {
        //            pickerSelectedString = @"";
        //    }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        let sectionWidth: Int = 300
        return CGFloat(sectionWidth)
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributedstr = NSAttributedString()
        attributedstr = NSAttributedString(string: dataArray[row] as! String, attributes: [NSAttributedStringKey.foregroundColor:UIColor.brown])
        return attributedstr
    }
}


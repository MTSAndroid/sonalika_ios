//
//  CustomClasses.swift
//  Moskeet
//
//  Created by Vijay on 15/03/18.
//  Copyright © 2018 TrakitNow. All rights reserved.
//

import Foundation
import UIKit
import Charts


class BarChart:BarChartView
{
    func setBarChartView(indexes:[String],dataPoints:[String],values:[Double]){
        
        self.clear()
        self.xAxis.labelPosition = .bottom
        self.chartDescription?.text = ""
        self.leftAxis.enabled = true
        self.leftAxis.labelPosition = .outsideChart
        self.rightAxis.enabled = false
        self.xAxis.drawGridLinesEnabled = false
        self.xAxis.labelCount = dataPoints.count
        self.xAxis.valueFormatter = IndexAxisValueFormatter(values: dataPoints)
        self.xAxis.granularity = 2.0
        self.animate(xAxisDuration: 1.0)
        self.animate(yAxisDuration: 1.0)
        self.highlightPerTapEnabled = true
        self.scaleXEnabled = false
        self.scaleYEnabled = false
        
        setChart(dataPoints: dataPoints, values: values)
        self.highlightValue(x: Double(indexes.count-1), dataSetIndex: 0, stackIndex: 0)
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        var dataEntries: [BarChartDataEntry] = []
      //  let utility = UtilityManager(dateService: dateUtility!)
        for i in 0..<dataPoints.count
        {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Hours")
        chartDataSet.setColor(.orange)
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.highlightEnabled = true
        if dataPoints.count <= 1
        {
            chartData.barWidth = 0.1
        }
        else
        {
            chartData.barWidth = 0.4
        }
        self.data = chartData
    }
}

/*class PieChart:PieChartView
{
    func setPieChartView(selectedObject:Species?) -> (Int,Int){
        
        self.clear()
        self.entryLabelColor = .cyan
        self.entryLabelFont = UIFont(name: kAppFont, size: 12.0)
        self.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        self.chartDescription?.text = kSpecies
        let details = selectedObject?.speciesDetail
        
        var species = [String]()
        var percentages = [Double]()
        var maleCount = 0
        var femaleCount = 0
        for dict in details!
        {
            species.append(dict.speciesType)
            let percent = Double(Double(dict.speciesTotal)/(selectedObject?.total)!)*100.0
            percentages.append(percent)
            for dict1 in dict.speciesDetails
            {
                if dict1.gender == "Male"
                {
                    maleCount = maleCount + dict1.count
                }
                else
                {
                    femaleCount = femaleCount + dict1.count
                }
            }
        }
        setPieChart(dataPoints: species, values: percentages)
        
        return (maleCount,femaleCount)
    }
    
    func setPieChart(dataPoints:[String],values:[Double])
    {
        var dataEntries: [PieChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = PieChartDataSet(values: dataEntries, label: "")
        chartDataSet.drawIconsEnabled = false
        chartDataSet.sliceSpace = 2.0;
        chartDataSet.iconsOffset = CGPoint(x: 0, y: 40)
        chartDataSet.entryLabelColor = .black
        chartDataSet.valueLineColor = .black
        
        var colors = [NSUIColor]()
        colors.append(.orange)
        
        if dataPoints.count > 1
        {
            colors.append(contentsOf: ChartColorTemplates.material())
        }
        chartDataSet.colors = colors
        
        let pieChartData = PieChartData(dataSet: chartDataSet)
        self.data = pieChartData
        self.data?.setValueFormatter(DefaultValueFormatter(formatter:appDelegate.setPercentFormatter()))
    }
}*/
class LineChart: LineChartView
{
    func setLineChartView(values:[Double],label: String)
    {
        self.clear()
        self.xAxis.labelPosition = .bottom
        self.chartDescription?.text = ""
        self.leftAxis.enabled = true
   //     self.leftAxis.calculate(min: 0, max: 15)
        self.leftAxis.labelPosition = .outsideChart
        self.rightAxis.enabled = false
        self.xAxis.drawGridLinesEnabled = false
        self.xAxis.labelCount = 10
        self.animate(xAxisDuration: 1.0)
        self.animate(yAxisDuration: 1.0)
        self.setChart(values: values,dates: [""],label: label)
    }
    func setChart(values:[Double],dates:[String],label:String)
    {
        var lineChartEntry = [ChartDataEntry]()
        let numbers = values
        print(numbers)
        for i in 0..<numbers.count
        {
            let values = ChartDataEntry(x: Double(i), y:Double(numbers[i]))
            lineChartEntry.append(values)
        }
        let line1 = LineChartDataSet(values: lineChartEntry, label:label )
        line1.colors = [NSUIColor.blue]
        line1.circleRadius = 0.1
        let data1 = LineChartData()
        data1.addDataSet(line1)
        self.data = data1
    }
}

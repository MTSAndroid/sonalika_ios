//
//  DatePicker.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

@objc protocol DatePickerProtocal: NSObjectProtocol {
    func datePickerSelectedDate(_ sender: Date?, forTag tag: Int)
}

class DatePicker: UIView {
    var selectedButton: UIView?
    weak var delegate: DatePickerProtocal?
    
    var datePicker: UIDatePicker?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setPickerWithTag(_ tag: Int) {
        datePicker?.tag = tag
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateformatter.timeZone = NSTimeZone.local
        if tag == 11 {
            if TabBarSingleTon.sharedObj.startDate != nil {
                let fromDate: Date? = dateformatter.date(from: TabBarSingleTon.sharedObj.startDate)
                if let aDate = dateformatter.date(from: TabBarSingleTon.sharedObj.endDate ?? "") {
                    datePicker?.date = aDate
                }
                datePicker?.minimumDate = Calendar.current.date(byAdding: .day, value: -7, to: Date())
            }
        } else {
            if TabBarSingleTon.sharedObj.endDate != nil {
                let toDate: Date? = dateformatter.date(from: TabBarSingleTon.sharedObj.endDate ?? "")
                if let aDate = dateformatter.date(from: TabBarSingleTon.sharedObj.startDate) {
                    datePicker?.date = aDate
                }
                datePicker?.maximumDate = toDate
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        baseInit(frame)
        
    }
    
    func baseInit(_ frame: CGRect) {
        let date = Date()
        datePicker = UIDatePicker(frame: CGRect(x: 0, y: 64, width: self.frame.size.width, height: self.frame.size.height - 64))
        datePicker?.datePickerMode = .dateAndTime
        datePicker?.isHidden = false
        datePicker?.maximumDate = Date()
        datePicker?.date = date
        let displayFormatter = DateFormatter()
        displayFormatter.timeZone = NSTimeZone.local
        displayFormatter.dateFormat = "MM月dd日 EEE HH:mm"
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local
        formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let pickerToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: 64))
        pickerToolbar.barStyle = .blackOpaque
        pickerToolbar.barTintColor = UIColor(red: 255/255, green: 237/255, blue: 0, alpha: 1.0)
        pickerToolbar.sizeToFit()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneBtn = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(DatePicker.datePickerDoneClick))
        doneBtn.tintColor = UIColor.white
        let itemArray = [flexSpace, doneBtn]
        pickerToolbar.setItems(itemArray, animated: true)
        addSubview(pickerToolbar)
        if let aPicker = datePicker {
            addSubview(aPicker)
        }
        backgroundColor = UIColor.white
    }
    
    @objc func datePickerDoneClick() {
        var selectedDate: Date? = datePicker?.date
        var otherDay: DateComponents? = nil
        if let aDate = selectedDate {
            otherDay = Calendar.current.dateComponents([.era, .year, .month, .day], from: aDate)
        }
        let today: DateComponents? = Calendar.current.dateComponents([.era, .year, .month, .day], from: Date())
        if today?.day == otherDay?.day && today?.month == otherDay?.month && today?.year == otherDay?.year && today?.era == otherDay?.era {
            if selectedButton?.tag == 100 {
                selectedDate = beginningOfDay(Date())
            } else {
                selectedDate = Date()
            }
        }
        if delegate?.responds(to: #selector(DatePickerProtocal.datePickerSelectedDate(_:forTag:))) ?? false {
            delegate?.datePickerSelectedDate(selectedDate, forTag: Int(datePicker?.tag ?? 0))
        }
        removeFromSuperview()
}
    func beginningOfDay(_ date: Date?) -> Date? {
        let cal = Calendar.current
        var components: DateComponents? = nil
        if let aDate = date {
            components = cal.dateComponents([.month, .year, .hour, .minute, .second, .day], from: aDate)
        }
        components?.hour = 0
        components?.minute = 0
        components?.second = 0
        if let aComponents = components {
            return cal.date(from: aComponents)
        }
        return nil
    }
}


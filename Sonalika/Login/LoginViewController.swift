//
//  LoginViewController.swift
//  Sonalika
//
//  Created by Manikumar on 24/10/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var loginleading: NSLayoutConstraint!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet var username:UITextField!
    @IBOutlet var password:UITextField!
    @IBOutlet weak var contentScrollView: UIScrollView!
    var keyBoardEndCalled = Bool()
    var lastSelectedTextField : UITextField!
    let appdelegate : AppDelegate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.keyBoardEndCalled = true
        loginBtn.layer.cornerRadius = (loginBtn.bounds.size.height)/2
        loginBtn.clipsToBounds = true
        usernameView.layer.cornerRadius = 5
        PasswordView.layer.cornerRadius = 5
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @IBAction func loginAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork()
        {
            self.view.endEditing(true)
            self.dismissKeyboard()
            if areAllFieldValid()
            {
                let obj = NetworkManager(utility: networkUtility!)
                let credentials = "\(username.text!):\(password.text!)"
                obj.loginApiAuthorization(credentials: credentials)
                {
                    (response,error) in
                    self.view.addActivity()
                    guard let dict = response as? NSDictionary
                    else
                    {
                        self.view.removeActivity()
                        return
                    }
                    let status = dict.value(forKey: "status") as? String
                    if status == "success"
                    {
                        self.view.removeActivity()
                        let userinfo = dict.value(forKey:"userInfo") as? NSDictionary
                        kUserDefaults.set(userinfo?.value(forKey: KToken), forKey: KToken)
                        kUserDefaults.set(userinfo?.value(forKey: KID), forKey: KID)
                        kUserDefaults.set(true, forKey: "logged_in")
                        kUserDefaults.set(userinfo?.value(forKey: KFirstName), forKey: KFirstName)
                        kUserDefaults.set(userinfo?.value(forKey: KLastName), forKey: KLastName)
                        kUserDefaults.set(userinfo?.value(forKey: KPhoneNumber), forKey: KPhoneNumber)
                        kUserDefaults.set(userinfo?.value(forKey: KEmail), forKey: KEmail)
                        kUserDefaults.set((userinfo?.value(forKey: KRoleID) as? NSArray)?.firstObject, forKey: KRoleID)
                        kUserDefaults.set((userinfo?.value(forKey: KroleCodes) as? NSArray)?.firstObject, forKey: KroleCodes)
                        kUserDefaults.synchronize()
                        self.registerDeviceforPushNotifications()
                        DispatchQueue.main.async {
                            let revealController = self.revealViewController()
                            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                            let navigationController = storyBoard.instantiateViewController(withIdentifier: "HomeNavigationController") as? UINavigationController
                            revealController?.setFront(navigationController, animated: true)
                        }
                        
                    }
                    else
                    {
                        self.showAlertWith(title: kFailure, message: status!)
                    }
                }
            }
        }
        else
        {
          showAlertWith(title: kFailure, message: kNetWorkFailureMessage)
        }
    }
    func registerDeviceforPushNotifications() {
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        if UserDefaults.standard.object(forKey: "deviceToken") != nil {
            if let aKey = UserDefaults.standard.object(forKey: "deviceToken") as? String {
                let obj = NetworkManager(utility: networkUtility!)
                obj.postApiWith(api:DeviceDataApi, params:["deviceId": deviceID!, "pushToken": aKey, "platform": "IOS"])
                {
                    (response) in
                    guard let response = response else
                    {
                        return
                    }
                    print(response)
                }
            }
        }
    }
    func setRootView(_ storyBoardName: String?, forStoryBoard name: String?) {
        let revealController: SWRevealViewController? = revealViewController()
        let storyBoard = UIStoryboard(name: storyBoardName ?? "", bundle: nil)
        let navigationController = storyBoard.instantiateViewController(withIdentifier: name!) as? UINavigationController
        revealController?.setFront(navigationController, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tecladoOFF(_:)), name: .UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.teclado(on:)), name: .UIKeyboardWillShow, object: nil)
      /*  if UI_USER_INTERFACE_IDIOM() == .pad
        {
            DispatchQueue.main.async {
                self.loginBtn.frame.size.width = (self.loginBtn.frame.size.width / 2)
                
                self.loginleading.constant = self.loginleading.constant + (self.loginBtn.frame.size.width / 2)
            }
            }*/

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        navigationController?.isNavigationBarHidden = false
        
    }
    @objc func teclado(on notification: Notification?) {
        if keyBoardEndCalled {
            if let userinfo = notification?.userInfo
            {
                keyBoardEndCalled = false
                let keyBoardheight = (userinfo[UIKeyboardFrameBeginUserInfoKey] as?NSValue)?.cgRectValue.size.height
                let height: CGFloat = contentScrollView.contentSize.height + (keyBoardheight ?? 0.0)
                contentScrollView.contentSize = CGSize(width: view.frame.size.width, height: height)
            }
        }
    }
    
    @objc func tecladoOFF(_ notification: Notification?) {
        if !keyBoardEndCalled {
            if let userinfo = notification?.userInfo
            {
                keyBoardEndCalled = true
                let keyBoardheight = (userinfo[UIKeyboardFrameBeginUserInfoKey] as?NSValue)?.cgRectValue.size.height
                let height: CGFloat = contentScrollView.contentSize.height - (keyBoardheight ?? 0.0)
                contentScrollView.contentSize = CGSize(width: view.frame.size.width, height: height)
            }
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        lastSelectedTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }

    
    func areAllFieldValid() -> Bool {
        
        username.text = username.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        if (username.text?.count)! < 1 {
            controls.presentAlertController("Please enter Username.", delegate: self)
            username.becomeFirstResponder()
            return false
        }
        
        password.text = password.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        if (password.text?.count)! < 1 {
            controls.presentAlertController("Please enter Password.", delegate: self)
            password.becomeFirstResponder()
            return false
        }
        return true
    }
    func dismissKeyboard() {
        if lastSelectedTextField != nil
        {
            if lastSelectedTextField.isFirstResponder {
                lastSelectedTextField.resignFirstResponder()
            }
        }
    }

    @IBAction func forgotPasswordAction(_ sender: Any) {
       
    }
    func showAlertWith(title:String,message:String)
    {
        let alertController = UIAlertController(title: title, message:message , preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.view.removeActivity()
            self.username.text = ""
            self.password.text = ""
            
        })
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

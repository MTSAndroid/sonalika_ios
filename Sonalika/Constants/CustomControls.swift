//
//  CustomControls.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 01/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class CustomControls
{
    static let sharedObj = CustomControls()
    func leftBarButtonMethod(_ ImgName: String?, selector: Selector, delegate del: Any?) -> [Any]? {
        let barButtonImage = (UIImage(named: ImgName ?? ""))?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0))
        let leftSlideButton = UIButton(type: .custom)
        leftSlideButton.frame = CGRect(x: 0.0, y: 0.0, width: barButtonImage?.size.width ?? 0.0, height: barButtonImage?.size.height ?? 0.0)
        leftSlideButton.backgroundColor = UIColor.clear
        leftSlideButton.contentHorizontalAlignment = .left
        leftSlideButton.setImage(barButtonImage, for: .normal)
        leftSlideButton.addTarget(del, action: selector, for: .touchUpInside)
        let leftBarButton = UIBarButtonItem(customView: leftSlideButton)
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        negativeSpacer.width = -5
        return [negativeSpacer, leftBarButton]
    }
    func showAlert(_ message: String?) {
//        let alert = UIAlertView(title: "", message: message!, delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "")
//        alert.show()
        /*UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
         
         }];
         UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         
         }];
         
         [alert addAction:okAction];
         [alert addAction:cancelAction];
         */
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: { action in
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
    }
    
    func presentAlertController(withTitle title: String?, message: String?, delegate: UIViewController, block response: @escaping (_ didCancel: Bool) -> Void) {
       
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                response(true)
            })
            alertController.addAction(ok)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                response(false)
            })
            alertController.addAction(cancel)
            delegate.present(alertController, animated: true, completion: nil)
    }
    
    func presentAlertController(_ message: String?, delegate: UIViewController) {
            let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(ok)
        (delegate).present(alertController, animated: true)
    }
    
    func rightBarButtonMethod(_ ImgName: String?, withTitle title: String?, selector: Selector, delegate del: Any?) -> UIBarButtonItem? {
        if ImgName != nil {
            let barButtonImage = (UIImage(named: ImgName ?? ""))?.resizableImage(withCapInsets: UIEdgeInsetsMake(0, 0, 0, 0))
            let rightSlideButton = UIButton(type: .custom)
            rightSlideButton.frame = CGRect(x: 0.0, y: 0.0, width: barButtonImage?.size.width ?? 0.0, height: barButtonImage?.size.height ?? 0.0)
            rightSlideButton.backgroundColor = UIColor.clear
            rightSlideButton.contentHorizontalAlignment = .left
            rightSlideButton.setImage(barButtonImage, for: .normal)
            rightSlideButton.addTarget(del, action: selector, for: .touchUpInside)
            if Float(UIDevice.current.systemVersion) ?? 0.0 >= 7.0 {
                rightSlideButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            } else {
                rightSlideButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0)
            }
            rightSlideButton.isUserInteractionEnabled = false
            let rightBarButton = UIBarButtonItem(customView: rightSlideButton)
            return rightBarButton
        } else {
            let rightBarButton = UIBarButtonItem(title: title, style: .plain, target: del, action: selector)
            return rightBarButton
        }
}
  
    func getCurrentDate() -> String? {
        let todayDate = Date()
        // get today date
        let dateFormatter = DateFormatter()
        // here we create NSDateFormatter object for change the Format of date..
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        //Here we can set the format which we need
        let convertedDateString = dateFormatter.string(from: todayDate)
        //
        return convertedDateString
    }
    
    func beginningOfDay(_ date: Date?) -> String? {
        let cal = Calendar.current
        var components: DateComponents? = nil
        if let aDate = date {
            components = cal.dateComponents([.month, .year, .hour, .minute, .second, .day], from: aDate)
        }
        components?.hour = 0
        components?.minute = 0
        components?.second = 0
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        var stratTimeDate: Date? = nil
        if let aComponents = components {
            stratTimeDate = cal.date(from: aComponents)
        }
        var dateString: String? = nil
        if let aDate = stratTimeDate {
            dateString = formatter.string(from: aDate)
        }
        return dateString
    }
    func endOfDay(_ date: Date?) -> String? {
        let cal = Calendar.current
        var components: DateComponents? = nil
        if let aDate = date {
            components = cal.dateComponents([.month, .year, .hour, .minute, .second, .day], from: aDate)
        }
        components?.hour = 23
        components?.minute = 59
        components?.second = 59
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        var endTimeDate: Date? = nil
        if let aComponents = components {
            endTimeDate = cal.date(from: aComponents)
        }
        var dateString: String? = nil
        if let aDate = endTimeDate {
            dateString = formatter.string(from: aDate)
        }
        return dateString
    }
    
    func getCurrentDateminusfiveHours() -> String? {
        var todayDate = Date()
        // get today date
        todayDate = todayDate.addingTimeInterval(-(330 * 60))
        let dateFormatter = DateFormatter()
        // here we create NSDateFormatter object for change the Format of date..
        dateFormatter.dateFormat = "yyyy-MM-dd  HH:mm:ss"
        //Here we can set the format which we need
        let convertedDateString = dateFormatter.string(from: todayDate)
        //
        return convertedDateString
    }
    func addFiveHoursthirtymins(_ sender: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        var date: Date? = dateFormatter.date(from: sender ?? "")
        if date == nil {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            date = dateFormatter.date(from: sender ?? "")
        }
        if date == nil {
            dateFormatter.dateFormat = "HH:mm:ss-dd/MM/yyyy"
            date = dateFormatter.date(from: sender ?? "")
        }
        date = date?.addingTimeInterval(+(330 * 60))
        var convertedDateString: String? = nil
        if let aDate = date {
            convertedDateString = dateFormatter.string(from: aDate)
        }
        return convertedDateString
    }
    
    func getFormattedDate(_ sender: String?) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date: Date? = formatter.date(from: sender ?? "")
        formatter.dateFormat = "dd/MM/yyyy"
        if let aDate = date {
            return formatter.string(from: aDate)
        }
        return nil
    }
    func getDate(_ sender: String?) -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let local = NSTimeZone.system as NSTimeZone
        formatter.timeZone = local as TimeZone
        var localstartDate: Date? = formatter.date(from: sender ?? "")
        if localstartDate == nil {
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
            localstartDate = formatter.date(from: sender ?? "")
        }
        var string: String? = nil
        if let aDate = localstartDate {
            string = formatter.string(from: aDate)
        }
        return string
    }
    
    func createLabelFrame(_ frame: CGRect, withTitle title: String?, with font: UIFont?, textColor color: UIColor?) -> UILabel? {
        let label = UILabel(frame: frame)
        label.backgroundColor = UIColor.clear
        if let aFont = font {
            label.font = aFont
        }
        if let aColor = color {
            label.textColor = aColor
        }
        label.text = title
        return label
    }
}

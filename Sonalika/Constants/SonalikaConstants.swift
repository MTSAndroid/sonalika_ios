//
//  MoskeetConstants.swift
//  Moskeet
//
//  Created by Vijay on 27/11/17.
//  Copyright © 2017 TrakitNow. All rights reserved.
//

import Foundation


let UI_IDIOM = UIDevice.current.userInterfaceIdiom
let appDelegate = UIApplication.shared.delegate as! AppDelegate
let APP_DELEGATE = UIApplication.shared.delegate as? AppDelegate
let kUserDefaults = UserDefaults.standard
let mainBundle = Bundle.main
let controls = CustomControls.sharedObj

// MARK:- Color codes & Fonts

let kAppNavColor = UIColor(red: 47/255, green: 99/255, blue: 204/255, alpha: 1.0)
let kAppFont = "Aleo"
let kNormalButtonColor = UIColor(red: 51/255, green: 49/255, blue: 50/255, alpha: 1.0)
let kSelectedButtonColor = UIColor(red: 252/255, green: 78/255, blue: 8/255, alpha: 1.0)
let NavigationBarColor = UIColor(red: 19.0 / 255.0, green: 69.0 / 255.0, blue: 252.0 / 255.0, alpha: 1)



// MARK:- Api keys

let kGoogleApiKey = "AIzaSyA93MBgR71sH0g1sD3uGQQlwhYFt52msUg"
let KGoogleWebServiceApiKey = "AIzaSyCLwT7DN9nsAulgaXCOV5JVXt-9WYQPWD4"
let kCrashlyticsApiKey = "786cd002227b4a31407db76f58962d4eca18da3b"

// MARK:- Api Urls



let kBaseUrl = "https://mynavi-sonalika.trakitnow.com/"

let ServerCompanyID = "SONALIKA"
let KToken = "token"
let KFirstName = "firstName"
let KLastName = "lastName"
let KPhoneNumber = "phone"
let KEmail = "email"
let KID = "id"
let KRoleID = "roleIDs"
let KroleCodes = "roleCodes"
let kApplicationId = "applicationId"
let kGet = "GET"
let kPost = "POST"
let kCustomerID = "2"
let kDealerID = "3"
let kServiceID = "5"
let kAdminID = "4"

let kMainDashboard = "Main"
let kDealerDashboard = "Dashboard"
let kDealerIdentifier = "DashboardView"
let kCustomerDashboard = "CustomerDashboard"
let kCustomerIdentifier = "CustomerDashboardView"
let kServiceManDashboard = "ServiceMan"
let kServiceManIdentifier = "ServiceManDashboardVC"
var apitoken = ""
let kDatapicker = "DatePickerView"
/*
/* Multiple Targets */
#if DEVELOPMENT
let kBaseUrl = "https://mynavi-sonalika.trakitnow.com/"
#elseif STAGING
let kBaseUrl = "https://moskeet.trakitnow.com/"
#else
let kBaseUrl = "https://moskeet.trakitnow.com/"
#endif
*/


let kLogin = "auth/myaccount/login"
let kgetdealer = "auth/user/edit?active=%@&extras.clientID=%@&limit=%@&page=%@&roleCodes=%@"
let klastenginedata = "mynavi/dvmap/list?limit=100&page=1&range={lastEngineOn:{<=:2018-10-30T18:29:59.000Z,>=:2018-10-26T18:30:00.000Z}}"
let VehiclesList = "mynavi/dvmap/list?customerID=%@&limit=1000&page=1"
let DealerVehiclesList = "mynavi/dvmap/list?dealerID=%@&limit=1000&page=1"
let VehiclesListAllroleIDs = "mynavi/dvmap/list?clientID=%@&limit=1000&page=1"

let DeviceDataApi = "mynavi/devicedata"
let kDevicesApi = "mapiuserdevices"
let kDeviceDailyApi = "mapidevicedata"
let kDeviceWeeklyApi = "mapidevicedataweek"
let kDeviceDataMonthApi = "mapidevicedatamonth"

// MARK:- SummaryDataApi (Daily/Weekly/Monthly/Yearly)

let kSummaryDataDailyApi = "mapiuserdevicesday"
let kSummaryDataWeeklyApi = "mapiuserdevicesweek"
let kSummaryDataMonthlyApi = "mapiuserdevicesmonth"
let kSummaryDataYearlyApi = "mapiuserdevicesyear"


// MARK:- Custom Keys

let kLoggedInKey = "loggedIn"
var isfromPetrolstation = false


// MARK:- case and height constants
let kCaseDashboard = 1
let kCaseDevicesdata = 2
let kCaseLogOut = 3
let kiPADCellHeight:CGFloat = 240.0
let kiPhoneCellHeight:CGFloat = 200.0



// MARK:- Static messages and titles

let kDaily = "Daily"
let kWeekly = "Weekly"
let kMonthly = "Monthly"
let kYearly = "Yearly"
let kSpecies = "Species"

let kTrapName = "trapName"
let kTrapIMEI = "trapIMEI"
let kLatitude = "latitude"
let kLongitude = "longitude"
let kLocation = "location"
let kAppTitle = "MOSKEET"
let kUsername = "username"
let kPassword = "password"

let kTypeYes = "YES"
let kTypeNo = "NO"
let kError = "error"
let kFailure = "Failure"
let kDeviceData = "device_data"
let kActivityView = "ActivityView"
let kSpeciesCountView = "SpeciesCountView"
let kRequiredDateFormat = "MM/dd/yyyy"
let kShortDateFormat = "dd-MMM"
let kYear = "year"
let kTotal = "total"
let kDetails = "details"
let kSpeciesTotal = "speciestotal"
let kSpeciesDetails = "speciesdetails"
let kGender = "gender"
let kCount = "count"

// MARK:- Messages
let kLogoutConfirmation = "Are you sure want to Logout?"
let kServerMessage = "Could not connected to Server."
let kLogout = "Logout"
let kNetWorkFailureMessage = "No Network.Please check your internet connection."


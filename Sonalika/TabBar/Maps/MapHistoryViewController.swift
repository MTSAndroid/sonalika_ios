//
//  MapHistoryViewController.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import GoogleMaps
import UIKit

class MapHistoryViewController: UIViewController, GMSMapViewDelegate {
    var mapView_: GMSMapView?
    
     var heartBeatArray = NSMutableArray()
     var delegate: AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Location History"
        view.backgroundColor = UIColor.lightGray
        getHeartBeat()
    }
    
    func drawPolyline() {
        let camera = GMSCameraPosition.camera(withLatitude: Double(TabBarSingleTon.sharedObj.vehicleDict.object(forKey: "lat") as! String)!, longitude: Double(TabBarSingleTon.sharedObj.vehicleDict.object(forKey: "lng") as! String)!, zoom: 13)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.delegate = self
        var bounds = GMSCoordinateBounds()
        let multiPath = GMSMutablePath()
        for i in 0..<(heartBeatArray.count) {
            let obj = heartBeatArray[i] as? HeartBeatObject
            multiPath.addLatitude((obj?.latitude)!, longitude: (obj?.longitude)!)
        }
        let polyline = GMSPolyline(path: multiPath)
        polyline.strokeColor = UIColor.purple
        polyline.strokeWidth = 5.0
        polyline.map = mapView
        
        let obj = heartBeatArray[0] as? HeartBeatObject
        let obj1 = heartBeatArray[heartBeatArray.count - 1] as? HeartBeatObject
        let sourceMarker = GMSMarker()
        sourceMarker.icon = GMSMarker.markerImage(with: UIColor.red)
        let latitude = obj?.latitude
        let longitude = obj?.longitude
        sourceMarker.appearAnimation = .pop
        sourceMarker.position = CLLocationCoordinate2DMake(latitude!, longitude!)
        bounds = bounds.includingCoordinate(sourceMarker.position)
        sourceMarker.map = mapView
        let destMarker = GMSMarker()
        destMarker.icon = GMSMarker.markerImage(with: UIColor.green)
        let latitude1 = obj1?.latitude
        let longitude1 = obj1?.longitude
        destMarker.appearAnimation = .pop
        destMarker.position = CLLocationCoordinate2DMake(latitude1!, longitude1!)
        bounds = bounds.includingCoordinate(destMarker.position)
        destMarker.map  = mapView
        self.view = mapView
    }
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        return true
    }
    
    func getHeartBeat() {
        heartBeatArray.removeAllObjects()
        delegate = UIApplication.shared.delegate as? AppDelegate
        
        let heartBeat = "/mynavi/heartbeat?deviceID=\(TabBarSingleTon.sharedObj.deviceID)&endDate=\((TabBarSingleTon.sharedObj.endDate))&limit=100&page= &sortBy=devicePublishTime&startDate=\((TabBarSingleTon.sharedObj.startDate))&vehicleNumber=\(TabBarSingleTon.sharedObj.vehicleDict.object(forKey: "vehicleNumber")!)"

        
        APP_DELEGATE?.showActivityIndicatorOnFullScreen()
        let obj = NetworkManager(utility: networkUtility!)
        obj.getNearsestPetrolStation(url: heartBeat)
        {
       
            (response)in
            if response != nil
            {
                guard let rows = ((response as? NSDictionary)?.value(forKey: "rows") as? NSArray)else
                {
                    APP_DELEGATE?.hideActivityIndcicator()
                    return
                }
                let obj = HeartBeatObject()
                for dict in rows
                {
                    let arrdict = (dict as? NSDictionary)
                    
                    if arrdict?.value(forKey: "lat") != nil
                    {
                        if arrdict?.object(forKey: "lat") is String
                        {
                          if arrdict?.object(forKey: "lat") as! String != ""
                          {
                            obj.latitude = Double((arrdict?.value(forKey: "lat") as? String)!)!
                          }
                          else
                          {
                                obj.latitude = 0.0
                          }
                          
                        }
                        else
                        {
                            obj.latitude = 0.0
                        }
                        
                    }
                    if arrdict?.value(forKey: "lng") != nil
                    {
                        if arrdict?.object(forKey: "lng") is String
                        {
                            if arrdict?.object(forKey: "lng") as! String != ""
                            {
                                obj.longitude = Double((arrdict?.value(forKey: "lng") as? String)!)!
                            }
                            else
                            {
                                obj.longitude = 0.0
                            }
                        }
                        else
                        {
                            obj.longitude = 0.0
                        }
                        
                    }
                    self.heartBeatArray.add(obj)
                }
                DispatchQueue.main.async {
                    if self.heartBeatArray.count > 0
                    {
                         self.drawPolyline()
                    }
                    else
                    {
                        let messageLabel = UILabel(frame: self.view.frame)
                        messageLabel.text = "No data available."
                        messageLabel.textAlignment = .center
                        self.view.addSubview(messageLabel)
                    }
                    APP_DELEGATE?.hideActivityIndcicator()
                }
            }
            else
            {
                OperationQueue.main.addOperation {
                    APP_DELEGATE?.hideActivityIndcicator()
                }
            }
        }
    }
}


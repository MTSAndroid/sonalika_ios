//
//  MapVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces
import UIKit

class MapVC: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate{
 //   var mapView_: GMSMapView?

    @IBOutlet var mapView_: GMSMapView!
    let vehicledict = TabBarSingleTon.sharedObj.vehicleDict
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let aModel = vehicledict.object(forKey: "deviceModel"), let aNumber = vehicledict.object(forKey: "vehicleNumber") {
            tabBarController?.title = "\(aModel) (\(aNumber))"
        }
        self.mapView_.delegate = self
        self.view.layoutIfNeeded()
        addGoogleMapView()
        drawPins()
    }
    
    func drawPins() {
        var bounds = GMSCoordinateBounds()
        let marker = GMSMarker()
        let latitude = Double(vehicledict.object(forKey: "lat") as! String)!
        let longitude = Double(vehicledict.object(forKey: "lng") as! String)!
        marker.position = CLLocationCoordinate2DMake(latitude , longitude )
        bounds = bounds.includingCoordinate(marker.position)
        marker.map = self.mapView_
    }
    
    func addGoogleMapView() {
        let camera = GMSCameraPosition.camera(withLatitude: (Double(vehicledict.object(forKey: "lat") as! String))!, longitude: (Double(vehicledict.object(forKey: "lng") as! String))!, zoom: 15.0)
        self.mapView_.camera = camera
        var frame: CGRect = view.frame
        frame.origin.y = 109
        frame.size.height = view.frame.size.height - 156
       // mapView_ = GMSMapView.map(withFrame: frame, camera: camera)
       // mapView_?.delegate = self
        mapView_?.settings.compassButton = true
//        if let a_ = mapView_ {
//            view.addSubview(a_)
//        }
        
}
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        if (Double(vehicledict.object(forKey: "lat") as! String)) != 0 && (Double(vehicledict.object(forKey: "lng") as! String)) != 0 {
            let mapHistory = MapHistoryViewController(nibName: nil, bundle: nil)
            navigationController?.pushViewController(mapHistory, animated: true)
            return true
        } else {
            let alertController = UIAlertController(title: "Location", message: "Location Undefined", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .destructive, handler: { action in
            })
            alertController.addAction(action)
            present(alertController, animated: true)
        }
        return false
    }
}

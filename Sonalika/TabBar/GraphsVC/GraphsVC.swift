//
//  GraphsVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit
import Charts

class GraphsVC: UIViewController, PickerViewProtocal,ChartViewDelegate,UITableViewDelegate,UITableViewDataSource{
 
    
    @IBOutlet weak var graphTypeButton: UIButton!

    fileprivate var popups: [UIView] = []
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var tableview: UITableView!
    
   
    private var heartBeatArray = NSMutableArray()
    private var delegate: AppDelegate?
    private var customPickerViewObj: CustomPickerView?
    private var pickerViewSelectedString = ""
    private var filterKey = ""
    private var selectedRow: Int = 0
    let lineChartView = LineChartView()
    let barchartview = BarChartView()
    var lbl = UILabel()
    let cellIdentifiers = ["LineCell","BarCell"]
    var chartValues = [Double]()
    var indexes = [String]()
    var dataPoints = [String]()
    var linedataPoints = [String]()
    var xindexes = [String]()
    var linechartvalues = [Double]()
    
    
  //  let labelSettings = ChartLabelSettings(font: GraphDefaults.labelFont)
    var lineData1:[(title: String, val:Double)] = []
    @IBAction func graphTypeAction(_ sender: Any) {
        let array = ["Battery Voltage level", "Fuel level", "RPM", "Coolant temparature", "Oil Pressure","Engine Hours"]
        if customPickerViewObj?.superview == nil {
            customPickerViewObj = CustomPickerView(frame: CGRect(x: 0, y: 150, width: view.frame.size.width, height: view.frame.size.height - 150))
            customPickerViewObj?.delegate = self
            customPickerViewObj?.dataArray = array
           
            customPickerViewObj?.setSelectedValue(selectedRow)
//            if let anObj = customPickerViewObj {
//                view.addSubview(anObj)
//            }
            self.view.addSubview(customPickerViewObj!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerViewSelectedString = "Battery Voltage level"
        selectedRow = 0
        filterKey = "batteryLevel"
        TabBarSingleTon.sharedObj.chartTypeString = "V"
        TabBarSingleTon.sharedObj.chartTypeFullString = pickerViewSelectedString
        delegate = UIApplication.shared.delegate as? AppDelegate
        if let aModel = delegate?.vehicleObject?.deviceModel, let aNumber = delegate?.vehicleObject?.vehicleNumber {
            tabBarController?.title = "\(aModel) (\(aNumber))"
        }
        //tableview.isScrollEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(GraphsVC.searchAction), name: NSNotification.Name("searchAction"), object: nil)
        
        getHeartBeat()
        getdaywiseEngineHours()
        
}
    
    @objc func searchAction()
 {
   self.getHeartBeat()
 }
  /*  func drawGraph() {
        print(self.heartBeatArray)
        if self.pickerViewSelectedString == "Engine Hours"
        {
            self.drawBarchat()
        }
        else
        {
        self.lineData1.removeAll()
        if lbl.superview != nil
        {
            self.lbl.removeFromSuperview()
        }
    
        for dict in heartBeatArray
        {
            print((dict as! HeartBeatObject).rpm)
            switch pickerViewSelectedString
            {
            case "Battery Voltage level":
                self.lineData1.append(("",(dict as! HeartBeatObject).batteryLevel))
                break
            case "Fuel level":
                self.lineData1.append(("",Double((dict as! HeartBeatObject).fuelLevel)!))
                break
            case "RPM":
                
                self.lineData1.append(("",Double((dict as! HeartBeatObject).rpm)))
                break
            case "Coolant temparature":
                self.lineData1.append(("",Double((dict as! HeartBeatObject).coolantTemp)!))
                break
//            case "Oil Pressure":
//                self.lineData1.append(("",(dict as! HeartBeatObject)))
//                break
            case "Engine Hours":
                self.drawBarchat()
                break
            default:
                break
            }
            
        }
        print(lineData1)

        
        let xGenerator = ChartAxisGeneratorMultiplier(1)
        let yGenerator = ChartAxisGeneratorMultiplier(3)
        let labelsGenerator = ChartAxisLabelsGeneratorFunc {scalar in
            return ChartAxisLabel(text: "\(scalar)", settings: self.labelSettings)
        }
        
        let xModel = ChartAxisModel(firstModelValue: 0, lastModelValue: 10, axisTitleLabels: [ChartAxisLabel(text: "", settings: labelSettings)], axisValuesGenerator: xGenerator, labelsGenerator: labelsGenerator)
        let yModel = ChartAxisModel(firstModelValue: 0, lastModelValue: 15, axisTitleLabels: [ChartAxisLabel(text: "", settings: labelSettings.defaultVertical())], axisValuesGenerator: yGenerator, labelsGenerator: labelsGenerator)
        
        let chartFrame = GraphDefaults.chartFrame(view.bounds)
        
        let chartSettings = GraphDefaults.chartSettingsWithPanZoom
        
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxisLayer, yAxisLayer, innerFrame) = (coordsSpace.xAxisLayer, coordsSpace.yAxisLayer, coordsSpace.chartInnerFrame)
        
        // create layer with guidelines
        let guidelinesLayerSettings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: GraphDefaults.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxisLayer: xAxisLayer, yAxisLayer: yAxisLayer, settings: guidelinesLayerSettings)
        
        let lineChartPoints = lineData1.enumerated().map {index, tuple in ChartPoint(x: ChartAxisValueDouble(index), y: ChartAxisValueDouble(tuple.val))}
        
        let lineModel = ChartLineModel(chartPoints: lineChartPoints, lineColor: UIColor.black, lineWidth: 2, animDuration: 0.5, animDelay: 1)
        let lineLayer = ChartPointsLineLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, lineModels: [lineModel])
        
        // circles layer
//        let circleViewGenerator = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
//            let color = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 1)
//            let circleView = ChartPointEllipseView(center: chartPointModel.screenLoc, diameter: 6)
//            circleView.animDuration = 0.5
//            circleView.fillColor = color
//            return circleView
//        }
        var selectedView: ChartPointTextCircleView?
        
        let circleViewGenerator = {[weak self] (chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in guard let weakSelf = self else {return nil}
            
            let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
            
            let v = ChartPointTextCircleView(chartPoint: chartPoint, center: screenLoc, diameter: Env.iPad ? 20 : 10, cornerRadius: Env.iPad ? 10: 5, borderWidth: Env.iPad ? 2 : 1, font: GraphDefaults.fontWithSize(Env.iPad ? 14 : 8))
            v.viewTapped = {view in
                for p in weakSelf.popups {p.removeFromSuperview()}
                selectedView?.selected = false
                
                let w: CGFloat = Env.iPad ? 250 : 150
                let h: CGFloat = Env.iPad ? 100 : 80
                
                if let chartViewScreenLoc = layer.containerToGlobalScreenLoc(chartPoint) {
                    let x: CGFloat = {
                        let attempt = chartViewScreenLoc.x - (w/2)
                        let leftBound: CGFloat = chart.bounds.origin.x
                        let rightBound = chart.bounds.size.width - 5
                        if attempt < leftBound {
                            return view.frame.origin.x
                        } else if attempt + w > rightBound {
                            return rightBound - w
                        }
                        return attempt
                    }()
                    
                    let frame = CGRect(x: x, y: chartViewScreenLoc.y - (h + (Env.iPad ? 30 : 12)), width: w, height: h)
                    
                    let bubbleView = InfoBubble(point: chartViewScreenLoc, frame: frame, arrowWidth: Env.iPad ? 40 : 28, arrowHeight: Env.iPad ? 20 : 14, bgColor: UIColor.black, arrowX: chartViewScreenLoc.x - x, arrowY: -1) // TODO don't calculate this here
                    chart.view.addSubview(bubbleView)
                    
                    bubbleView.transform = CGAffineTransform(scaleX: 0, y: 0).concatenating(CGAffineTransform(translationX: 0, y: 100))
                    let infoView = UILabel(frame: CGRect(x: 0, y: 10, width: w, height: h - 30))
                    infoView.textColor = UIColor.white
                    infoView.backgroundColor = UIColor.black
                    infoView.text = "BatteryLevel \(chartPoint)V"
                    infoView.font = GraphDefaults.fontWithSize(Env.iPad ? 14 : 12)
                    infoView.textAlignment = NSTextAlignment.center
                    
                    bubbleView.addSubview(infoView)
                    weakSelf.popups.append(bubbleView)
                    
                    UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                        view.selected = true
                        selectedView = view
                        
                        bubbleView.transform = CGAffineTransform.identity
                    }, completion: {finished in})
                }
            }
            
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: UIViewAnimationOptions(), animations: {
                let w: CGFloat = v.frame.size.width
                let h: CGFloat = v.frame.size.height
                let frame = CGRect(x: screenLoc.x - (w/2), y: screenLoc.y - (h/2), width: w, height: h)
                v.frame = frame
            }, completion: nil)
            
            return v
        }
        let lineCirclesLayer = ChartPointsViewsLayer(xAxis: xAxisLayer.axis, yAxis: yAxisLayer.axis, chartPoints: lineChartPoints, viewGenerator: circleViewGenerator, displayDelay: 1.5, delayBetweenItems: 0.05, mode: .translate)
        
        let chart = Chart(
            frame: chartFrame,
            innerFrame: innerFrame,
            settings: chartSettings,
            layers: [
                xAxisLayer,
                yAxisLayer,
                lineLayer,
                lineCirclesLayer,
                guidelinesLayer
            ]
        )
        self.graphView.addSubview(chart.view)
        self.chart = chart
        
        }
    }*/
    
    func drawBarchat()
    {
        let dataPoints = ["1","2","3","4","5"]
        let indexes = ["1","2","3","4","5"]
        let values = [100.0,200.0,300.0,400.0,500.0]
        barchartview.delegate = self
        barchartview.clear()
        barchartview.xAxis.labelPosition = .bottom
        barchartview.chartDescription?.text = ""
        barchartview.leftAxis.enabled = true
        barchartview.leftAxis.labelPosition = .outsideChart
        barchartview.rightAxis.enabled = false
        barchartview.xAxis.drawGridLinesEnabled = false
        barchartview.xAxis.labelCount = dataPoints.count
        barchartview.xAxis.valueFormatter = IndexAxisValueFormatter(values: indexes)
        barchartview.animate(xAxisDuration: 1.0)
        barchartview.animate(yAxisDuration: 1.0)
        barchartview.highlightPerTapEnabled = true
        barchartview.scaleXEnabled = false
        barchartview.scaleYEnabled = false
        self.setChart(dataPoints: dataPoints, values: values)
    }
    func setChart(dataPoints: [String], values: [Double]) {
        var dataEntries: [BarChartDataEntry] = []
      //  let utility = UtilityManager(dateService: dateUtility!)
        for i in 0..<dataPoints.count
        {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Hours")
        chartDataSet.setColor(.orange)
        
        let chartData = BarChartData(dataSet: chartDataSet)
        chartData.highlightEnabled = true
        if dataPoints.count <= 1
        {
            chartData.barWidth = 0.1
        }
        else
        {
            chartData.barWidth = 0.4
        }
        self.barchartview.data = chartData
        DispatchQueue.main.async {
            for view in self.graphView.subviews {
                view.removeFromSuperview()
            }
            self.graphView.addSubview(self.barchartview)
            self.graphView.bringSubview(toFront: self.barchartview)
        }
    }

    
    func getHeartBeat() {
        self.heartBeatArray.removeAllObjects()
       // let heartBeat = "/mynavi/heartbeat?deviceID=\(TabBarSingleTon.sharedObj.deviceID)&endDate=\(TabBarSingleTon.sharedObj.endDate)&limit=1000&page= 1&sortBy=createdAt&startDate=\(TabBarSingleTon.sharedObj.startDate)"
        let enddate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let enddateminus5 = dateFormatter.string(from: enddate)
        let startdate = controls.addFiveHoursthirtymins(enddateminus5)
        
        let heartBeat = "mynavi/heartbeat?deviceID=\(TabBarSingleTon.sharedObj.deviceID)&endDate=\(TabBarSingleTon.sharedObj.startDate)&page=1&sortBy=createdAt&startDate=\(TabBarSingleTon.sharedObj.endDate)&limit=10"
        APP_DELEGATE?.showActivityIndicatorOnFullScreen()
        let obj = NetworkManager(utility: networkUtility!)
        obj.getNearsestPetrolStation(url: heartBeat)
        {
            (response)in
            if response != nil
            {
                guard let rows = ((response as? NSDictionary)?.value(forKey: "rows") as? NSArray)else
                {
                    APP_DELEGATE?.hideActivityIndcicator()
                    return
                }
                var i = 0
                if rows.count > 0
                {
                    let obj = HeartBeatObject()
                    for dict in rows
                    {
                        i += 1
                        let objdict = dict as? NSDictionary
                        let extradict = objdict?.value(forKey: "extras") as? NSDictionary
                        obj.time = (objdict?.value(forKey: "createdAt") as? String)!
                        if self.checkKeyExists(dict: extradict!, key: "fuelLevel")
                        {
                     //    obj.fuelLevel = String((extradict?.value(forKey: "fuelLevel") as? String)!)
                        obj.fuelLevel = "6.588"
                        print(obj.fuelLevel)
                        }
                        else
                        {
                          obj.fuelLevel = "0.0"
                        }
                        
                        if self.checkKeyExists(dict: extradict!, key: "rpm")
                        {
                           // obj.rpm = String(extradict?.value(forKey: "rpm") as? Double)!
                            obj.rpm = "7.54"
                        }
                        else
                        {
                           obj.rpm = "0.0"
                        }
                        
                        if self.checkKeyExists(dict: extradict!, key: "fuelStatus")
                        {
                             obj.opsStatus = (extradict?.value(forKey: "fuelStatus") as? String)!
                        }
                        else
                        {
                            obj.opsStatus = ""
                        }
                      
                        if self.checkKeyExists(dict: extradict!, key: "coolantTemp")
                        {
                          obj.coolantTemp = String((extradict?.value(forKey: "coolantTemp") as? Int)!)
                        }
                        else
                        {
                         obj.coolantTemp = ""
                        }
                        if self.checkKeyExists(dict: extradict!, key: "batteryLevel")
                        {
                          if (extradict?.value(forKey: "batteryLevel") is String)
                          {
                            obj.batteryLevel = Double((extradict?.value(forKey: "batteryLevel") as? String)!)!
                          }
                          else if (extradict?.value(forKey: "batteryLevel") is Double)
                          {
                            obj.batteryLevel = (extradict?.value(forKey: "batteryLevel") as? Double)!
                            }
                            else
                          {
                            obj.batteryLevel = 0.0
                          }
                         
                        }
                        else
                        {
                          obj.batteryLevel = 0.0
                        }
                        if self.checkKeyExists(dict: extradict!, key: "devicePublishTime")
                        {
                           obj.devicePublishTime = (extradict?.value(forKey: "devicePublishTime") as? String)!
                        }
                        else
                        {
                           obj.devicePublishTime = ""
                        }
                        self.linedataPoints.append("A" + "\(((dict as? NSDictionary)?.count)! + i)")
                        self.xindexes.append("A" + "\(((dict as? NSDictionary)?.count)! + i)")
                        self.lineData1.append(("A",obj.batteryLevel))
                        self.heartBeatArray.add(obj)
                    }
               
                }
                DispatchQueue.main.async {
//                    if self.chartView.superview != nil {
//                        self.chartView.removeFromSuperview()
//                    }
                    if rows.count > 0
                    {
                        self.drawGraph()
                        
                        self.tableview.reloadData()
                       
                    }
                    else
                    {
                        self.lbl = UILabel(frame: self.graphView.frame)
                        self.lbl.text = "No Data Avaliable"
                        self.lbl.textAlignment = .center
                        self.graphView.addSubview(self.lbl)
                    }
                    APP_DELEGATE?.hideActivityIndcicator()
                    
                }
            }
        }
    }
    func drawGraph()
    {
      for dict in heartBeatArray
      {
        self.linechartvalues.append((dict as! HeartBeatObject).batteryLevel)
      }
    }
    func getdaywiseEngineHours()
    {
       let url =  "mynavi/engineHourByDateWise?deviceID=\(TabBarSingleTon.sharedObj.deviceID)&endDate=\(TabBarSingleTon.sharedObj.startDate)&limit=1000&page=1&sortBy=createdAt&startDate=\(TabBarSingleTon.sharedObj.endDate)"
       let obj = NetworkManager(utility: networkUtility!)
     
       obj.getNearsestPetrolStation(url: url)
       {
            (response) in
             guard let result = response as? NSDictionary else
             {
                    return
             }
            let status = (result.object(forKey: "status") as? String)
            if  status == "success"
            {
            let rows = result.object(forKey: "rows") as? NSArray
                for i in rows!
                {
                let dict = (i as? NSDictionary)
                    self.indexes.append((dict?.object(forKey:"date") as? String)!)
                    self.dataPoints.append((dict?.object(forKey:"date") as? String)!)
                    let arr = self.secondsToHoursMinutesSeconds(seconds:(dict?.object(forKey: "runHours") as? Int)!)
                   let runhours = arr.0 + (arr.1 / 100)
                self.chartValues.append(Double(runhours))
            }
                DispatchQueue.main.async {
                    self.tableview.reloadData()
                }
        }
        
       }
       
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Double, Double, Double) {
        return (Double(seconds / 3600), Double((seconds % 3600) / 60), Double((seconds % 3600) % 60))
    }
    func checkKeyExists(dict:NSDictionary,key:String) -> Bool
    {
        if dict.value(forKey: key) != nil
        {
            return true
        }
        else
        { return false
        }
    }
  
    func fliter(withKey sender: String?) -> NSArray? {
        let matches = heartBeatArray.map { ($0 as AnyObject).sender }
        var tempArray = matches as NSArray
        if (sender == "devicePublishTime") {
            if let anArray = convertString(toDateFormat: tempArray as! NSMutableArray as? [AnyHashable]) {
                if let anArray = convertString(toDateFormat: tempArray as! NSMutableArray as? [AnyHashable]) {
                    tempArray = anArray as NSArray
                }
            }
        }
        return tempArray as NSArray
    }
    
    func convertString(toDateFormat sender: [AnyHashable]?) -> [AnyHashable]? {
        var sender = sender
        for i in 0..<(sender?.count ?? 0) {
            let str = sender?[i] as? String
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            let date: Date? = dateFormatter.date(from: str ?? "")
            dateFormatter.dateFormat = "yyyy-MM-dd  HH:mm:ss"
            var convertedDateString: String? = nil
            if let aDate = date {
                convertedDateString = dateFormatter.string(from: aDate)
            }
            sender?[i] = convertedDateString ?? ""
        }
        return sender
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.indexes.count > 0 ? cellIdentifiers.count : 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (self.view.bounds.height - 200)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifiers[indexPath.section], for: indexPath)
        switch indexPath.section {
        case 0:
            guard let chartview = (cell.viewWithTag(10) as? LineChart)
                else{
                    break
            }
            chartview.delegate = self
            chartview.setLineChartView(values: self.linechartvalues,label: pickerViewSelectedString)
            break
        case 1:
            guard let chartview = (cell.viewWithTag(11) as? BarChart)
                else{
                    break
            }
            let dataPoints = self.dataPoints
            let indexes = self.indexes
            let values = self.chartValues
            chartview.delegate = self
            chartview.setBarChartView(indexes: indexes, dataPoints: dataPoints, values: values)
            break
        default:
            break
        }
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1
        {
            return "Engine Hours"
        }
        else
        {
            return pickerViewSelectedString
        }
    }
    
    func pickerViewSelectedValue(_ sender: String?, forRow row: Int) {
        if sender?.count != 0 {
            pickerViewSelectedString = sender!
            selectedRow = row
            TabBarSingleTon.sharedObj.chartTypeFullString = pickerViewSelectedString
            graphTypeButton.setTitle(sender, for: .normal)
            if (pickerViewSelectedString == "Battery Voltage level") {
                filterKey = "batteryLevel"
                TabBarSingleTon.sharedObj.chartTypeString = "V"
            } else if (pickerViewSelectedString == "Fuel level") {
                filterKey = "fuelLevel"
                TabBarSingleTon.sharedObj.chartTypeString = ""
            } else if (pickerViewSelectedString == "RPM") {
                filterKey = "rpm"
                TabBarSingleTon.sharedObj.chartTypeString = "RPM"
            } else if (pickerViewSelectedString == "Coolant temparature") {
                filterKey = "coolantTemp"
                TabBarSingleTon.sharedObj.chartTypeString = "C"
            } else if (pickerViewSelectedString == "Oil Pressure") {
                filterKey = "opsStatus"
                TabBarSingleTon.sharedObj.chartTypeString = "bar"
            }
            else if (pickerViewSelectedString == "Engine Hours") {
                filterKey = "enginehours"
                TabBarSingleTon.sharedObj.chartTypeString = "bar"
            }
            self.tableview.reloadData()
            if pickerViewSelectedString == "Engine Hours"
            {
                      self.tableview.scrollToRow(at: IndexPath.init(row: 0, section: 1), at: .none, animated: true)
            }
          //  drawGraph()
     
        }
    }
    fileprivate func removePopups() {
        for popup in popups {
            popup.removeFromSuperview()
        }
    }
    func onZoom(scaleX: CGFloat, scaleY: CGFloat, deltaX: CGFloat, deltaY: CGFloat, centerX: CGFloat, centerY: CGFloat, isGesture: Bool) {
        removePopups()
    }
    
    func onPan(transX: CGFloat, transY: CGFloat, deltaX: CGFloat, deltaY: CGFloat, isGesture: Bool, isDeceleration: Bool) {
        removePopups()
    }
    

}

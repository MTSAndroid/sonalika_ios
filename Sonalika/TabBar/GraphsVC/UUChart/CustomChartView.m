//
//  popUpView.m
//  MyNavi
//
//  Created by sridhar on 26/05/17.
//  Copyright © 2017 Ramakrishna. All rights reserved.
//

#import "CustomChartView.h"
#define kArrowHeight 50

@implementation CustomChartView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
  
    return self;

}

- (void)drawRect:(CGRect)rect
{
CGContextRef context = UIGraphicsGetCurrentContext();

UIBezierPath *fillPath = [UIBezierPath bezierPath];
[fillPath moveToPoint:CGPointMake(0, self.frame.origin.y+kArrowHeight)];
[fillPath addLineToPoint:CGPointMake(self.frame.size.width/2-(kArrowHeight/2), kArrowHeight)];
[fillPath addLineToPoint:CGPointMake(self.frame.size.width/2, 0)];
[fillPath addLineToPoint:CGPointMake(self.frame.size.width/2+(kArrowHeight/2), kArrowHeight)];
    [fillPath addLineToPoint:CGPointMake(self.frame.size.width, kArrowHeight)];
    [fillPath addLineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height)];
    [fillPath addLineToPoint:CGPointMake(0, self.frame.size.height)];
    [fillPath closePath];
    
    CGContextAddPath(context, fillPath.CGPath);
    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextFillPath(context);
    
}

@end

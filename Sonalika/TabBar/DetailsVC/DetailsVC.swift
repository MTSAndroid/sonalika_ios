//
//  DetailsVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class DetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var listView: UITableView!
    
    private var titlesArray = NSArray()
    private var detailsArray =  NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let delegate = UIApplication.shared.delegate as? AppDelegate
        tabBarController?.title = "Details"
        titlesArray = ["Vehicle Number :", "Model Code :",         /*@"Device Number :",*/
            "Engine Last On :", "Total Engine Hours :", "Last Data Captured On :", "Last Location :"]
        let vehicledict1 = TabBarSingleTon.sharedObj.vehicleDict
        
     /*   detailsArray = [delegate?.vehicleObject?.vehicleNumber, delegate?.vehicleObject?.deviceModel,         /*delegate.vehicleObject.deviceID,*/
            delegate?.vehicleObject?.engineLastOn, delegate?.vehicleObject?.totalEngineHours, delegate?.vehicleObject?.lastDataReceivedAt, delegate?.vehicleObject?.vehicleLocation]*/
        detailsArray = [vehicledict1.object(forKey: "vehicleNumber"),vehicledict1.object(forKey: "deviceModel"),vehicledict1.object(forKey: "lastEngineOn"),vehicledict1.object(forKey:"totalEngineHours"),vehicledict1.object(forKey:"lastDataReceivedAt"),vehicledict1.object(forKey: "vehicleLocation")]
                listView.rowHeight = UITableViewAutomaticDimension
        listView.estimatedRowHeight = 140
        listView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (titlesArray.count > 0 ? titlesArray.count:0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DetailsCell
        if cell == nil {
            cell = DetailsCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        cell?.titleLable.text = titlesArray[indexPath.row] as? String
        if detailsArray[indexPath.row] is NSNull
        {
          cell?.detailLable.text = "NA"
        }
        else
        {
            cell?.detailLable.text = detailsArray[indexPath.row] as? String
        }
        return cell!
}
}

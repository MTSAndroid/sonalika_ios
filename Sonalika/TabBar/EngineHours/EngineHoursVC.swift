//
//  EngineHoursVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation

import UIKit

class EngineHoursVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var engineHoursGridView: UICollectionView!
    
     var delegate: AppDelegate?
     var engineHoursArray = NSMutableArray()
     var pageNumber: Int = 0
    var Norecords = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageNumber = 1
        delegate = UIApplication.shared.delegate as? AppDelegate
        if let aModel = delegate?.vehicleObject?.deviceModel, let aNumber = delegate?.vehicleObject?.vehicleNumber {
            tabBarController?.title = "\(aModel) (\(aNumber))"
        }
        NotificationCenter.default.addObserver(self, selector: #selector(EngineHoursVC.searchAction), name: NSNotification.Name("searchAction"), object: nil)
      //  getEngineHours()
    }
    
    @objc func searchAction() {
        //    if([TabBarSingleTon.sharedObj compareDates]){
        pageNumber = 1
        engineHoursArray.removeAllObjects()
       // getEngineHours()
        /*   }
         else
         {
         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:@"From date should be less than or equal to to date." preferredStyle:UIAlertControllerStyleAlert];
         UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
         
         }];
         [alertController addAction:action];
         [self presentViewController:alertController animated:YES completion:nil];
         
         }*/
    }
    
    func insertRowAtBottom() {
        if pageNumber != Norecords {
            pageNumber += 1
           // getEngineHours()
        }
//        } else {
//            engineHoursGridView.infiniteScrollingView.stopAnimating()
//        }
    }
    
    func getEngineHours() {
        
        let engineHours = "/mynavi/enginehours?deviceID=\((delegate?.vehicleObject?.deviceID)!)&endDate=\(TabBarSingleTon.sharedObj.endDate)&limit=20&page=\(pageNumber)&startDate=\(TabBarSingleTon.sharedObj.startDate)&vehicleNumber=\((delegate?.vehicleObject?.vehicleNumber)!)"
        if engineHoursArray.count == 0 {
            APP_DELEGATE?.showActivityIndicatorOnFullScreen()
        }
        let obj = NetworkManager(utility: networkUtility!)
        obj.getNearsestPetrolStation(url: engineHours)
        {
            (response) in
            if response != nil
            {
                guard let rows = ((response as? NSDictionary)?.value(forKey: "rows") as? NSArray)
                else
                {
                    return
                }
                if rows.count > 0
                {
                    let obj = EngineHoursObject()
                    for dict in rows
                    {
                        let objdict = dict as? NSDictionary
                        obj.startDate = objdict?.value(forKey:"onTimestamp") as! String
                        obj.endDate = objdict?.value(forKey:"offTimestamp") as! String
                        obj.hours = objdict?.value(forKey:"engineHours") as! String
                        self.engineHoursArray.add(obj)
                    }
                }
                else
                {
                    self.pageNumber = self.Norecords
                }
                DispatchQueue.main.async {
                    self.engineHoursGridView.reloadData()
                    APP_DELEGATE?.hideActivityIndcicator()
                }
            }
        }
}

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 5, 5, 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat
        if UI_USER_INTERFACE_IDIOM() == .pad
        {
            height = 64
        }
        else
        {
            height = 44
        }
        return CGSize(width: collectionView.frame.width / 3 - 10, height: height)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if engineHoursArray.count != 0 {
            collectionView.backgroundView = nil
            return engineHoursArray.count
        } else {
            let messageLabel = UILabel(frame: collectionView.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            collectionView.backgroundView = messageLabel
        }
        return 0
    }
    
    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
}
    func collectionView(_ collectionview: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? EngineHoursCell
        let obj = engineHoursArray[indexPath.section] as? EngineHoursObject
        cell?.dateLable.textAlignment = .center
        if indexPath.row % 3 == 0 {
            cell?.dateLable.text = obj?.startDate
        } else if indexPath.row % 3 == 1 {
            cell?.dateLable.text = obj?.endDate
        } else {
            cell?.dateLable.text = obj?.hours
        }
       
        return cell!
    }
    
    func getDateString(_ sender: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date: Date? = dateFormatter.date(from: sender ?? "")
        dateFormatter.dateFormat = "dd/MM/yyyy\nHH:mm:ss a"
        var convertedDateString: String? = nil
        if let aDate = date {
            convertedDateString = dateFormatter.string(from: aDate)
        }
        return convertedDateString
    }
}

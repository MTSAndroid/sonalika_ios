//
//  TabBarSingleTon.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import CoreLocation
import Foundation
import UIKit

class TabBarSingleTon: NSObject {
    var startDate = ""
    var endDate = ""
    var chartTypeString = ""
    var chartTypeFullString = ""
    var deviceID = ""
    var vehicleDict = NSDictionary()
    var vlat = ""
    var vlng = ""
    
    static let sharedObj = TabBarSingleTon()
    func compareDates() -> Bool {
        var ret:Bool?
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateformatter.timeZone = NSTimeZone.local
        let fromDate: Date? = dateformatter.date(from: startDate)
        let toDate: Date? = dateformatter.date(from: endDate)
        if let aDate = toDate {
            if ((fromDate?.compare(aDate)) != nil) || fromDate?.compare(aDate) == .orderedAscending {
                ret = true
            } else {
                ret = false
            }
        }
        return ret!
    }
}

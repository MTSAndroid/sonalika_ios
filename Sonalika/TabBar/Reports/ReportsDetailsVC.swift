//
//  ReportsDetailsVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class ReportDetailsVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var listView: UITableView!
    var heartBeatObject: HeartBeatObject?
    
    var translations = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        listView.rowHeight = UITableViewAutomaticDimension
        listView.estimatedRowHeight = 140
        listView.tableFooterView = UIView()
        let rightbarBtn: UIBarButtonItem? = CustomControls.sharedObj.rightBarButtonMethod(nil, withTitle: "Close", selector: #selector(ReportDetailsVC.popAction), delegate: self)
        rightbarBtn?.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = rightbarBtn
        
//        var dict1: [AnyHashable : Any]? = nil
//        if let aLevel = heartBeatObject?.batteryLevel {
//            dict1 = [
//                "label" : "Battery Level",
//                "status" : aLevel
//            ]
//        }
//        if let aDict1 = dict1 {
//            translations.add(aDict1)
//        }
//        if let aTemp = heartBeatObject?.coolantTemp {
//            if !("\(aTemp)" == "(null)") {
//                var dict2: [AnyHashable : Any]? = nil
//                if let aTemp = heartBeatObject?.coolantTemp {
//                    dict2 = [
//                        "label" : "Coolent Temperature",
//                        "status" : "\(aTemp) \u{2103}"
//                    ]
//                }
//                if let aDict2 = dict2 {
//                    translations.add(aDict2)
//                }
//            }
//        }
//        if let aFuelpersentage = heartBeatObject?.fuelpersentage {
//            if !("\(aFuelpersentage)" == "(null)") {
//                var dict3: [AnyHashable : Any]? = nil
//                if let aFuelpersentage = heartBeatObject?.fuelpersentage {
//                    dict3 = [
//                        "label" : "Fuel Percentage",
//                        "status" : "\(aFuelpersentage) \("%")"
//                    ]
//                }
//
}
    
    @objc func popAction()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (translations.count > 0 ? translations.count:0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? DetailsCell
        if cell == nil {
            cell = DetailsCell(style: .default, reuseIdentifier: cellIdentifier)
        }
        let dict = translations[indexPath.row] as? [AnyHashable : Any]
        cell?.titleLable.text = dict?["label"] as! String
        cell?.detailLable.text = dict?["status"] as! String
        
        return cell!
    }
}

//
//  HeartBeatObject.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation

class HeartBeatObject: NSObject {
    var time = String()
    var fuelLevel = String()
    var rpm = String()
    var coolantTemp = String()
    var batteryLevel = Double()
    var ignitionStatus = String()
    var opsStatus = String()
    var travelSpeed = String()
    var parkingSwitch = String()
    var gearStatus = String()
    var key = String()
    var keyStatus = String()
    var hydraulicOilFilterStatus = String()
    var devicePublishTime = String()
    var fuelpersentage = String()
    var latitude = Double()
    var longitude = Double()
    var translations = NSArray()
}

//
//  ReportsVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 03/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class ReportsVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var reportsGridView: UICollectionView!
    @IBOutlet weak var alertButton: UIButton!
    
     var previousBtn: UIButton?
     var delegate = TabBarSingleTon.sharedObj
     var reportsArray = NSMutableArray()
     var viewtype = ""
     var pageNumber: Int = 0
     var isAlertType = false
     var Norecords = -1
    
    @IBAction func selectReportsType(_ sender: UIButton) {
        if !(sender.titleLabel?.text == previousBtn?.titleLabel?.text) {
            if (sender.titleLabel?.text == "Alert View") {
                sender.setImage(UIImage(named: "bullet_selection"), for: .normal)
                previousBtn?.setImage(UIImage(named: "bullet-1"), for: .normal)
                viewtype = "findalerts"
                isAlertType = true
            } else {
                sender.setImage(UIImage(named: "bullet_selection"), for: .normal)
                previousBtn?.setImage(UIImage(named: "bullet-1"), for: .normal)
                viewtype = "heartbeat"
                isAlertType = false
            }
            reportsArray.removeAllObjects()
            previousBtn = sender
            pageNumber = 1
            getHeartBeat()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewtype = "heartbeat"
        pageNumber = 1
        previousBtn = alertButton
        
        
        if let aModel = delegate.vehicleDict.object(forKey: "deviceModel"), let aNumber = delegate.vehicleDict.object(forKey: "vehicleNumber") {
            tabBarController?.title = "\(aModel) (\(aNumber))"
        }
        NotificationCenter.default.addObserver(self, selector: #selector(ReportsVC.searchAction), name: NSNotification.Name("searchAction"), object: nil)
        getHeartBeat()
    }
    
    @objc func searchAction() {
        //    if([TabBarSingleTon.sharedObj compareDates]){
        pageNumber = 1
        reportsArray.removeAllObjects()
        getHeartBeat()
    }
    func insertRowAtBottom() {
        if pageNumber != Norecords {
            pageNumber += 1
            getHeartBeat()
        } else {
         //   self.reportsGridView.infiniteScrollingView.stopAnimating()
        }
    }
    
    func getHeartBeat() {
       // let heartBeat = String(format: Reports, viewtype, (delegate?.vehicleObject!.deviceID)!, TabBarSingleTon.sharedObj.endDate, pageNumber, TabBarSingleTon.sharedObj.startDate, (delegate?.vehicleObject?.vehicleNumber)!)
        let heartBeat = "/mynavi/\(viewtype)?deviceID=\((delegate.deviceID))&endDate=\(TabBarSingleTon.sharedObj.endDate)&limit=15&page=\(pageNumber)&sortBy=createdAt&startDate=\(TabBarSingleTon.sharedObj.startDate)&vehicleNumber=\((delegate.vehicleDict.object(forKey:"vehicleNumber"))!)"
      
        if reportsArray.count == 0 {
            APP_DELEGATE?.showActivityIndicatorOnFullScreen()
        }
        let obj = NetworkManager(utility: networkUtility!)
        obj.getNearsestPetrolStation(url: heartBeat)
        {
            (response)in
            if response != nil
            {
                guard let rows = ((response as? NSDictionary)?.value(forKey: "rows") as? NSArray) else
                {
                    return
                }
                if rows.count > 0
                {
                    for dict in rows
                    {
                        let arrdict = dict as? NSDictionary
                        let extradict = arrdict?.value(forKey: "extras") as? NSDictionary
                        let obj = HeartBeatObject()
                        obj.time = (arrdict?.value(forKey: "devicePublishTime") as? String ?? "")
                        if let aKey = extradict?["ignitionStatus"] {
                            if ("\(aKey)" == "0") {
                                obj.ignitionStatus = "OFF"
                            } else {
                                obj.ignitionStatus = "ON"
                            }
                        }
                        if let aKey = extradict?["travelSpeed"] {
                            obj.travelSpeed = "\(aKey)"
                        }
                        if let aKey = extradict?["opsStatus"] {
                            obj.opsStatus = "\(aKey) bar"
                        }
                        if let aKey = extradict?["parkingSwitch"] {
                            obj.parkingSwitch = "\(aKey)"
                        }
                        if let aKey = extradict?["gearStatus"] {
                            obj.gearStatus = "\(aKey)"
                        }
                        if let aKey = extradict?["fuelStatus"] {
                            if "\(aKey)" == ""
                            {
                                obj.fuelLevel = "NA"
                            }
                            else
                            {
                                obj.fuelLevel = "\(aKey)"
                            }
                        }
                        else
                        {
                            obj.fuelLevel = "NA"
                        }
                        obj.rpm = "\(trunc(Double(extradict?.value(forKey: "rpm") as? String ?? "0.0")!))"
                        if extradict?["coolantTemp"] != nil {
                            if let aKey = extradict?["coolantTemp"] {
                                if let aKey = extradict?["coolantTemp"] {
                                    obj.coolantTemp = "\(aKey)"
                                }
                            }
                        }
                        if let aKey = extradict?["batteryLevel"] {
                            if !("\(aKey)" == "(null)") {
                                if let aKey = extradict?["batteryLevel"] {
                                 //   obj.batteryLevel = "\(aKey) V"
                                }
                            } else {
                             //   obj.batteryLevel = "0 V"
                            }
                        }
                        if extradict?["keyStatus"] != nil {
                            if let aKey = extradict?["keyStatus"] {
                                obj.keyStatus = "\(aKey)"
                            }
                        }
                        if extradict?["fuelpersentage"] != nil {
                            if let aKey = extradict?["fuelpersentage"] {
                                obj.fuelpersentage = "\(aKey)"
                            }
                        }
                        if extradict?["hydraulicOilFilterStatus"] != nil {
                            if let aKey = extradict?["hydraulicOilFilterStatus"] {
                                obj.hydraulicOilFilterStatus = "\(aKey)"
                            }
                        }
                        if (arrdict!["translations"] as? NSArray)?.isEqual(NSNull()) ?? false {
                            //  obj.translations = [Any]()
                        } else {
                            obj.translations = arrdict?["translations"] as! NSArray
                        }
                        self.reportsArray.add(obj)
                    }
                }
                else
                {
                    self.pageNumber = self.Norecords
                }
                DispatchQueue.main.async {
    
                    self.reportsGridView.reloadData()
                    APP_DELEGATE?.hideActivityIndcicator()
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5, 0, 5, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat
        if UI_USER_INTERFACE_IDIOM() == .pad
        {
            height = 64
        }
        else
        {
            height = 44
        }
        return CGSize(width: collectionView.frame.width / 5 - 10, height: height)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if reportsArray.count != 0 {
            collectionView.backgroundView = nil
            return reportsArray.count
        } else {
            let messageLabel = UILabel(frame: collectionView.frame)
            messageLabel.text = "No data available."
            messageLabel.textAlignment = .center
            collectionView.backgroundView = messageLabel
        }
        return 0
    }
    
    func collectionView(_ view: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    func collectionView(_ collectionview: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionview.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ReportsCell
        let obj = reportsArray[indexPath.section] as? HeartBeatObject
        if indexPath.row % 5 == 0 {
            cell?.dateLable.text = getDateString(obj?.time)
        } else if indexPath.row % 5 == 1 {
            if let aLevel = obj?.ignitionStatus {
                if "\(aLevel)" == "ON"
                {
                    cell?.dateLable.textColor = UIColor.red
                }
                if "\(aLevel)" == "OFF"
                {
                    cell?.dateLable.textColor = UIColor.green
                }
                cell?.dateLable.text = "\(aLevel)"
            }
            if (cell?.dateLable.text == "(null)") {
                cell?.dateLable.text = "NA"
            }
        } else if indexPath.row % 5 == 2 {
            cell?.dateLable.text = "\((obj?.rpm)!)"
        } else if indexPath.row % 5 == 3 {
            cell?.dateLable.text = "\((obj?.coolantTemp)!)"
        } else if indexPath.row % 5 == 4 {
            if let aLevel = obj?.batteryLevel {
                if let aLevel = obj?.batteryLevel {
                    cell?.dateLable.text = "\((aLevel))"
                }
            }
        }
        if indexPath.row % 5 == 0 {
            cell?.dateLable.textColor = UIColor(red: 0.4862, green: 0.5764, blue: 0.7450, alpha: 1)
        }
        else if indexPath.row % 5 == 1
        {
            if cell?.dateLable.text == "ON"
            {
                cell?.dateLable.textColor = UIColor.red
            }
            if cell?.dateLable.text == "OFF"
            {
                cell?.dateLable.textColor = UIColor.green
            }
        }
        else {
            cell?.dateLable.textColor = UIColor.black
        }
        if let aCell = cell {
            return aCell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.section == collectionView.numberOfSections - 1 &&
            indexPath.row == collectionView.numberOfItems(inSection: indexPath.section) - 1 {
            self.insertRowAtBottom()
        }
    }
    
    func getDateString(_ sender: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date: Date? = dateFormatter.date(from: sender ?? "")
        dateFormatter.dateFormat = "yyyy/MM/dd\nHH:mm"
        var convertedDateString: String? = nil
        if let aDate = date {
            convertedDateString = dateFormatter.string(from: aDate)
        }
        return convertedDateString
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //    NSArray *translations = [(HeartBeatObject*)[reportsArray objectAtIndex:indexPath.section] translations];
        if indexPath.row % 5 == 0 {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let reportDetailsVCObj = storyBoard.instantiateViewController(withIdentifier: "ReportDetailsVC") as? ReportDetailsVC
            if isAlertType {
                reportDetailsVCObj?.title = "Alert Status"
            } else {
                reportDetailsVCObj?.title = "Detailed Status"
            }
            reportDetailsVCObj?.heartBeatObject = reportsArray[indexPath.section] as? HeartBeatObject
            var navi: UINavigationController? = nil
            if let anObj = reportDetailsVCObj {
                navi = UINavigationController(rootViewController: anObj)
            }
            if let aNavi = navi {
                navigationController?.present(aNavi, animated: true)
            }
}
    
}
}

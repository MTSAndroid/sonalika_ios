//
//  TabViewController.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class TabViewController: UITabBarController, UITabBarControllerDelegate, DatePickerProtocal, UINavigationControllerDelegate {
    var tag: Int = 0
    var vehicleObject: VehicleObject?
    
    var datesView: DatesView?
    var datePickerView: DatePicker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        navigationItem.leftBarButtonItems = CustomControls.sharedObj.leftBarButtonMethod("arrow_left", selector: #selector(TabViewController.popAction(_:)), delegate: self) as? [UIBarButtonItem]
        tabBar.barTintColor = UIColor.white
        UITabBarItem.appearance().setTitleTextAttributes([
            .font : UIFont.systemFont(ofSize: 11),
            .foregroundColor : UIColor.gray
            ], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([
            .font : UIFont.systemFont(ofSize: 11),
            .foregroundColor : UIColor.red
            ], for: .selected)
        APP_DELEGATE?.vehicleObject = vehicleObject
        // Date View
        datesView = Bundle.main.loadNibNamed("DatesView", owner: self, options: nil)?.first as? DatesView
        datesView?.frame = CGRect(x: 0, y: 67, width: view.frame.size.width, height: 40)
        setDatesIntialvalue(datesView?.fromDate)
        let fromDateGesture = UITapGestureRecognizer(target: self, action: #selector(TabViewController.datePickerAction(_:)))
        fromDateGesture.numberOfTapsRequired = 1
        datesView?.fromDate.tag = 10
        datesView?.fromDate.addGestureRecognizer(fromDateGesture)
        let toDateGesture = UITapGestureRecognizer(target: self, action: #selector(TabViewController.datePickerAction(_:)))
        toDateGesture.numberOfTapsRequired = 1
        datesView?.toDate.tag = 11
        datesView?.toDate.addGestureRecognizer(toDateGesture)
        datesView?.searchButton.addTarget(self, action: #selector(TabViewController.searchAction(_:)), for: .touchUpInside)
        if view.frame.size.width <= 320.0 {
            (datesView?.toDate.subviews[1] as? UILabel)?.font = UIFont.systemFont(ofSize: 12.0)
            (datesView?.fromDate.subviews[1] as? UILabel)?.font = UIFont.systemFont(ofSize: 12.0)
        }
        datesView?.loadData()
//        if let aView = datesView {
//            view.addSubview(aView)
//        }
        self.view.addSubview(datesView!)
      //  hideDatesView()
        UITabBarItem.appearance().setTitleTextAttributes([
            .font : UIFont.boldSystemFont(ofSize: 12),
            .foregroundColor : NavigationBarColor
            ], for: .normal)
        getOriginalImages()
        moreNavigationController.navigationBar.barTintColor = UIColor.orange
        moreNavigationController.view.tintColor = UIColor(red: 29.0 / 255.0, green: 9.0 / 255.0, blue: 91.0 / 255.0, alpha: 1)
}
    func getOriginalImages() {
        let detailsTabBarItem: UITabBarItem? = tabBar.items?[0]
        detailsTabBarItem?.selectedImage = UIImage(named: "list-outline_sel")?.withRenderingMode(.alwaysOriginal)
        detailsTabBarItem?.image = detailsTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
        let mapsTabBarItem: UITabBarItem? = tabBar.items?[1]
        mapsTabBarItem?.selectedImage = UIImage(named: "location_sel")?.withRenderingMode(.alwaysOriginal)
        mapsTabBarItem?.image = mapsTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
        let graphsTabBarItem: UITabBarItem? = tabBar.items?[2]
        graphsTabBarItem?.selectedImage = UIImage(named: "arrow-graph-up-right_sel")?.withRenderingMode(.alwaysOriginal)
        graphsTabBarItem?.image = graphsTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
        let reportsTabBarItem: UITabBarItem? = tabBar.items?[3]
        reportsTabBarItem?.selectedImage = UIImage(named: "document-text_sel")?.withRenderingMode(.alwaysOriginal)
        reportsTabBarItem?.image = reportsTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
        let engineTabBarItem: UITabBarItem? = tabBar.items?[4]
        engineTabBarItem?.selectedImage = UIImage(named: "cog-outline_sel")?.withRenderingMode(.alwaysOriginal)
        engineTabBarItem?.image = engineTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
       /* let batchTabBarItem: UITabBarItem? = tabBar.items?[5]
        batchTabBarItem?.selectedImage = UIImage(named: "document-text_sel")?.withRenderingMode(.alwaysOriginal)
        batchTabBarItem?.image = batchTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
        let consumptionTabBarItem: UITabBarItem? = tabBar.items?[6]
        consumptionTabBarItem?.selectedImage = UIImage(named: "document-text_sel")?.withRenderingMode(.alwaysOriginal)
        consumptionTabBarItem?.image = consumptionTabBarItem?.image?.withRenderingMode(.alwaysOriginal)
        let analyticsTabBarItem: UITabBarItem? = tabBar.items?[7]
        analyticsTabBarItem?.selectedImage = UIImage(named: "arrow-graph-up-right_sel")?.withRenderingMode(.alwaysOriginal)
        analyticsTabBarItem?.image = analyticsTabBarItem?.image?.withRenderingMode(.alwaysOriginal)*/
    }
    
    @objc func searchAction(_ sender: UIButton?) {
        NotificationCenter.default.post(name: NSNotification.Name("searchAction"), object: nil)
    }
    
    func setDatesIntialvalue(_ sender: UIView?) {
        var selectedDate = Date()
        let threedays = Calendar.current.date(byAdding: .day, value: -4, to: selectedDate)
        let otherDay: DateComponents? = Calendar.current.dateComponents([.era, .year, .month, .day], from: threedays!)
        let today: DateComponents? = Calendar.current.dateComponents([.era, .year, .month, .day], from: selectedDate)
    /*    if today?.day == otherDay?.day && today?.month == otherDay?.month && today?.year == otherDay?.year && today?.era == otherDay?.era {
            if sender?.isEqual(datesView?.fromDate) ?? false {
                if let aDate = beginningOfDay(Date()) {
                    selectedDate = aDate
                }
            } else {
                selectedDate = Date()
            }
        }*/
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        var dateString = formatter.string(from: selectedDate)
        var endString = formatter.string(from: threedays!)
//        if sender?.isEqual(datesView?.fromDate) ?? false {
//            TabBarSingleTon.sharedObj.startDate = endString
//        } else {
//            TabBarSingleTon.sharedObj.endDate = dateString
//        }
        TabBarSingleTon.sharedObj.endDate = dateString
        TabBarSingleTon.sharedObj.startDate = endString
        formatter.dateFormat = "MMM dd, yyyy"
        dateString = formatter.string(from: selectedDate)
        endString = formatter.string(from: threedays!)
        (datesView?.toDate.subviews[1] as? UILabel)?.text = dateString
        (datesView?.fromDate.subviews[1] as? UILabel)?.text = endString
    }
    func beginningOfDay(_ date: Date?) -> Date? {
        let cal = Calendar.current
        var components: DateComponents? = nil
        if let aDate = date {
            components = cal.dateComponents([.month, .year, .hour, .minute, .second, .day], from: aDate)
        }
        components?.hour = 0
        components?.minute = 0
        components?.second = 0
        if let aComponents = components {
            return cal.date(from: aComponents)
        }
        return nil
    }
    
    @objc func datePickerAction(_ sender: UITapGestureRecognizer?) {
        if datePickerView?.superview == nil {
            datePickerView = DatePicker(frame: CGRect(x: 0, y: 110, width: view.frame.size.width, height: view.frame.size.height - 110))
            datePickerView?.selectedButton = sender?.view
            datePickerView?.delegate = self
            
            datePickerView?.setPickerWithTag(Int(sender?.view?.tag ?? 0))
            if let aView = datePickerView {
                view.addSubview(aView)
            }
        }
    }
    
    func datePickerSelectedDate(_ sender: Date?, forTag tag: Int) {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = "MMM dd, yyyy"
        var dateString: String? = nil
        if let aSender = sender {
            dateString = formatter.string(from: aSender)
        }
        (datePickerView?.selectedButton?.subviews[1] as? UILabel)?.text = dateString
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let aSender = sender {
            dateString = formatter.string(from: aSender)
        }
        if datePickerView?.selectedButton == datesView?.fromDate {
            TabBarSingleTon.sharedObj.startDate = dateString ?? ""
        } else {
            TabBarSingleTon.sharedObj.endDate = dateString!
        }
        datePickerView = nil
    }
    
    func tabBarController(_ theTabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let indexOfTab: Int? = (theTabBarController.viewControllers as NSArray?)?.index(of: viewController)
        if indexOfTab == 0 {
            hideDatesView()
            
        } else if (viewController.isKind(of: moreNavigationController.superclass!)) {
            title = "More"
            if viewController == moreNavigationController {
                moreNavigationController.delegate = self
            }
        } else {
            showDatesView()
        }
}
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if (viewController.title == "More") {
            hideDatesView()
        } else {
            showDatesView()
        }
    }
    
    func hideDatesView() {
        datesView?.isHidden = true
    }
    
    func showDatesView() {
        datesView?.isHidden = false
    }
    
    func image(from color: UIColor?, for size: CGSize, withCornerRadius radius: CGFloat) -> UIImage? {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor((color?.cgColor)!)
        context?.fill(rect)
        var image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIGraphicsBeginImageContext(size)
        UIBezierPath(roundedRect: rect, cornerRadius: radius).addClip()
        image?.draw(in: rect)
        image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    @objc func popAction(_ sender: Any?) {
        navigationController?.popViewController(animated: true)
    }
}

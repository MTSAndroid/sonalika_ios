//
//  NetworkManager.swift
//  Sonalika
//
//  Created by Manikumar on 21/02/18.
//  Copyright © 2018 TrakitNow. All rights reserved.
//

import UIKit

final class NetworkManager: NSObject
{
    private let networkUtility:NetworkUtility
    
    // Injecting object
    init(utility:NetworkUtility) {
        self.networkUtility = utility
    }
    
    // Post request with 'String' type params
    func postApiWith(api:String,params:[String:String],completion:@escaping(_ data:Any?)->Void)
    {
         networkUtility.postRequest(api: api, parameters: params, completion: completion)
    }
    
    // Post request with 'Any' type params
    func postRequestApiWith(api:String,params:[String:Any],completion:@escaping(_ data:Any?)->Void){
        
        networkUtility.postWithRequest(api: api, parameters: params, completion: completion)
    }
    
    // Get request with 'String' type params
    func getRequestApiWith(api:String,params:[String:Any],completion:@escaping(_ data:Any?,_ error:Error?)->Void)
    {
        networkUtility.getRequest(api: api, parameters: params as! [String : String], completion: completion)
    }
    
    //Post request for login
    func loginApiAuthorization(credentials:String,completion:@escaping(_ data:Any?, _ error:Error?)->Void)
    {
        networkUtility.getloginRequest(credentials: credentials, completion: completion)
    }
    
    // To get Nearest Petrol Stations
    func getNearsestPetrolStation(url:String,completion:@escaping(_ data:Any?)-> Void)
    {
        networkUtility.getdataforPlacesAPI(url: url, completion: completion)
    }
}

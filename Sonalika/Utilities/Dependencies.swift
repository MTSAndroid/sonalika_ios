//
//  ViewModel.swift
//  Sonalika
//
//  Created by Manikumar on 16/02/18.
//  Copyright © 2018 TrakitNow. All rights reserved.
//

/********************* Dependency Injection Implementation ********************/

import UIKit
import Alamofire
import SwiftyJSON


//MARK:- DateModel Protocol

protocol DateModel {
    
    var currentDate:String{get}
    var currentYear:String{get}
    var currentMonth:String{get}
    var currentWeek:String{get}
}

//MARK:- NetworkModel Protocol

protocol NetworkModel {
    
    // Post Request with 'String' Type parameters
    func postRequest(api:String,parameters:[String:String],completion:@escaping(_ data:Any?)->Void)
    
    // Post Request with 'Any' type parameters
    func postWithRequest(api:String,parameters:[String:Any],completion:@escaping(_ data:Any?)->Void)

    //Get request with 'String' type parmeters
    func getRequest(api:String,parameters:[String:String],completion:@escaping(_ data:Any?,_ error:Error?)->Void)
    
    //Get login Request with encrpted details
    func getloginRequest(credentials:String,completion:@escaping(_ data:Any?,_ error:Error?)->Void)
    
}

//MARK:- Date Model Protocol Inheritance

final class DateUtility:DateModel{
    
    var currentDate: String{
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = kRequiredDateFormat
        return dateformatter.string(from:Date())
    }
    
    var currentWeek: String{
        
        return "\(Calendar.current.component(.weekOfYear, from: Date()))"
    }
    
    var currentMonth: String{
        
        return "\(Calendar.current.component(.month, from: Date()))"
    }
    
    var currentYear: String{
        
        return "\(Calendar.current.component(.year, from: Date()))"
    }
}

var dateUtility:DateUtility? = DateUtility()

//MARK:- Network Model Protocol Inheritance

final class NetworkUtility:NetworkModel{
    
    func postRequest(api: String, parameters: [String : String], completion: @escaping (Any?) -> Void) {
        
        let url = URL(string:"\(kBaseUrl)\(api)")
        let token = (kUserDefaults.object(forKey: KToken) as! String)
        let headers = [
            "companyID": ServerCompanyID,
            "Content-Type": "application/json",
            "Authorization": "Token \(token)"
        ]
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    
    func postWithRequest(api: String, parameters: [String : Any], completion: @escaping (Any?) -> Void) {
        
        let url = URL(string:"\(kBaseUrl)\(api)")
        
        
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil)
            }
            else
            {
                completion(response.result.value!)
            }
        }
    }
    
    func getRequest(api: String, parameters: [String : String], completion: @escaping (Any?,Error?) -> Void) {
        
        let urlstr = kBaseUrl + api
        let mainurl = urlstr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string:mainurl!)
        let token = (kUserDefaults.object(forKey: KToken) as! String)
        let headers = [
            "companyID": ServerCompanyID,
            "Content-Type": "application/json",
            "Authorization": "Token \(token)"
        ]
        Alamofire.request(url!, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON { (response) in
            
            if response.result.isFailure
            {
                completion(nil, (response.result.debugDescription as? Error))
            }
            else
            {
                completion(response.result.value!,nil)
            }
        }
    }
    func getloginRequest(credentials:String,completion:@escaping(_ data:Any?,_ error:Error?)->Void)
    {
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        let baseurl = kBaseUrl + kLogin
        
        let mainurl = baseurl.replacingOccurrences(of:" ", with:"%20")
        var request = URLRequest(url: URL(string:"\(mainurl)")!)
        request.addValue(ServerCompanyID, forHTTPHeaderField: "companyID")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let data = credentials.data(using: .utf8, allowLossyConversion: .init())
        let encodedCredentials = data?.base64EncodedData()
        let Strcredentials = String(data: encodedCredentials!, encoding: .utf8)
        let authHeader = "Basic \(Strcredentials ?? "")"
        request.addValue(authHeader, forHTTPHeaderField: "Authorization")
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: [], options: .prettyPrinted)
        }
        catch(let jsonError){
            print(jsonError.localizedDescription)
        }
        let task = session.dataTask(with: request) { (data, response, error) in
            
            if error == nil
            {
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                    completion(json,nil)
                }
                catch(let jsonerror){
                    print(jsonerror.localizedDescription)
                    completion(nil,error!)
                }
            }
            else
            {
                print(error!)
            }
        }
        task.resume()
    }
    func getdataforPlacesAPI(url:String,completion:@escaping(_ data:Any?)->Void)
    {
        var bUrl = url
        if !isfromPetrolstation
        {
            bUrl = kBaseUrl + url
        }
        let mainurl = bUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url1 = NSURL(string: mainurl!)
        var urlRequest = URLRequest(url: url1! as URL)
        urlRequest.httpMethod = kGet
    
        urlRequest.addValue("Token \((kUserDefaults.object(forKey: KToken) as? String)!)", forHTTPHeaderField: "Authorization")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue(ServerCompanyID, forHTTPHeaderField: "companyID")
        Alamofire.request(urlRequest)
            .responseJSON { response in
                if response.response != nil
                {
                    completion(response.result.value)
                }
                else
                {
                    completion(nil)
                }
        }
        isfromPetrolstation = false
    }
    
}
var networkUtility:NetworkUtility? = NetworkUtility()
    




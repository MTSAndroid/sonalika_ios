//
//  AppDelegate.swift
//  Sonalika
//
//  Created by Manikumar on 24/10/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import Fabric
import Crashlytics
import GoogleMaps
import GooglePlaces
import UserNotifications
import Alamofire



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var revealController = SWRevealViewController()
    var badge: Int = 0
    var backgroundView: UIView?
    var btnBackgroundView: UIView?
    var deviceTokenString = ""
    var activityView: UIActivityIndicatorView?
    var vehicleObject: VehicleObject?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Crashlytics.start(withAPIKey: "786cd002227b4a31407db76f58962d4eca18da3b")
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey(kGoogleApiKey)
        GMSPlacesClient.provideAPIKey(kGoogleApiKey)
        var textAttributes:NSDictionary?
        var font: UIFont?
      
        if UI_USER_INTERFACE_IDIOM() == .pad {
            font = UIFont.systemFont(ofSize: 26)
        } else {
            font = UIFont.systemFont(ofSize: 18)
        }
        
        UINavigationBar.appearance().titleTextAttributes = textAttributes as? [NSAttributedStringKey : Any]
        UINavigationBar.appearance().barTintColor = NavigationBarColor
        if !UserDefaults.standard.bool(forKey: "logged_in") {
            setStoryBoardWithName(kMainDashboard, withIdentifier: "LoginNavigationController")
        } else {
//            if ((kUserDefaults.value(forKey: KRoleID) as? String) == kDealerID) {
//                setStoryBoardWithName(kDealerDashboard, withIdentifier: kDealerIdentifier)
//            } else if ((kUserDefaults.value(forKey: KRoleID)as? String) == kCustomerID) {
//                setStoryBoardWithName(kCustomerDashboard, withIdentifier: kCustomerIdentifier)
//            } else {
//                setStoryBoardWithName(kServiceManDashboard, withIdentifier: kServiceManIdentifier)
//            }
            setStoryBoardWithName(kMainDashboard, withIdentifier: "HomeNavigationController")
        }
        let center = UNUserNotificationCenter.current()
        center.delegate = self as? UNUserNotificationCenterDelegate
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if error == nil {
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            }
        })
//        let currentLocation = LocaitonManager()
//        print(currentLocation)
        loguser()
        registerForPushNotifications()
        return true
    }
    func loguser() {
        if let aName = kUserDefaults.value(forKey: KFirstName), let aName1 = kUserDefaults.value(forKey: KLastName){
            Crashlytics.setUserName("\(aName) + \(aName1)")
        }
        Crashlytics.setUserEmail(kUserDefaults.value(forKey: KEmail) as? String)
        Crashlytics.setUserIdentifier(kUserDefaults.value(forKey: KID) as? String)
    }
    func setStoryBoardWithName(_ name: String?, withIdentifier identifier: String?) {
        let leftStoryBoard = UIStoryboard(name: kMainDashboard, bundle: nil)
        let slidMenu: UIViewController? = leftStoryBoard.instantiateViewController(withIdentifier: "LeftViewController")
        var navigationController: UINavigationController?
        if (name == kMainDashboard) {
            navigationController = leftStoryBoard.instantiateViewController(withIdentifier: identifier!) as? UINavigationController
        } else {
            let storyBoard = UIStoryboard(name: name ?? "", bundle: nil)
            navigationController = storyBoard.instantiateViewController(withIdentifier: identifier!) as? UINavigationController
        }
        revealController = SWRevealViewController(rearViewController: slidMenu, frontViewController: navigationController)
        window?.rootViewController = revealController
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Userinfo \(response.notification.request.content.userInfo)")
    }
    
   
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        kUserDefaults.set(token, forKey: "deviceToken")
        
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
   

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Sonalika")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func pop(toViewController identifier: String?) {
        let storyBoard = UIStoryboard(name: kMainDashboard, bundle: nil)
        let slidMenu: UIViewController? = storyBoard.instantiateViewController(withIdentifier: "LeftViewController")
        var navigationController: UINavigationController?
        navigationController = storyBoard.instantiateViewController(withIdentifier: identifier!) as? UINavigationController
        revealController = SWRevealViewController(rearViewController: slidMenu, frontViewController: navigationController)
        window?.rootViewController = revealController
    }
    
    func showActivityIndicatorOnFullScreen() {
        if !(backgroundView != nil) {
            backgroundView = UIView(frame: (window?.frame)!)
            backgroundView?.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
            activityView = UIActivityIndicatorView(activityIndicatorStyle: .white)
            backgroundView?.addSubview(activityView!)
            activityView?.center = (backgroundView?.center)!
        }
        DispatchQueue.main.async(execute: {
            if (self.activityView?.isAnimating)! {
                self.activityView?.stopAnimating()
            }
            self.activityView?.startAnimating()
        })
        window?.addSubview(backgroundView!)
    }
    
    func hideActivityIndcicator() {
        if ((backgroundView?.superview) != nil) {
            backgroundView?.removeFromSuperview()
        }
    }
    
}


//
//  LeftViewController.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class LeftViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var nameLable: UILabel!
    @IBOutlet var phoneNumberLable: UILabel!
    @IBOutlet var designationLable: UILabel!
    @IBOutlet var versionLable: UILabel!
    var customerViewController:UIViewController!
    
     var menuArray = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMenuArray()
        self.revealViewController().rearViewRevealWidth = 300
        self.revealViewController().rearViewRevealOverdraw = 0
        self.revealViewController().rearViewRevealDisplacement = 0
        if let aName = kUserDefaults.value(forKey: KFirstName), let aName1 = kUserDefaults.value(forKey: KLastName) {
            nameLable.text = "\(aName) \(aName1)"
        }
        phoneNumberLable.text = kUserDefaults.value(forKey: KPhoneNumber) as? String
        designationLable.text = kUserDefaults.value(forKey: KEmail) as? String
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (menuArray.count > 0 ? menuArray.count:0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? LeftMenuCell
        if cell == nil {
            cell = LeftMenuCell(style: .default, reuseIdentifier: cellIdentifier)
            if UI_USER_INTERFACE_IDIOM() == .pad {
                cell?.imgView.frame = CGRect(x: cell?.imgView.frame.origin.x ?? 0.0, y: 17, width: 36, height: 36)
                let width: CGFloat = (cell?.imgView.frame.origin.x)! + (cell?.imgView.frame.size.width)! + 10
                cell?.titleLable.frame = CGRect(x: width, y: 0, width: cell?.superview?.frame.size.width ?? 0.0 - width, height: 70)
            }
        }
        let obj = menuArray[indexPath.row] as? MenuObject
        cell?.imgView.image = UIImage(named: obj?.imageName ?? "")
        cell?.titleLable.text = obj?.title
        return cell!
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            return 70
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let revealController = SWRevealViewController()
        switch indexPath.row {
        case 0:
            
            let storyBoard = UIStoryboard(name: kMainDashboard, bundle: nil)
            let nearbyLocationsMapViewController = storyBoard.instantiateViewController(withIdentifier: "HomeNavigationController") as? UINavigationController
            
            revealController.setFront(nearbyLocationsMapViewController, animated: true)
            revealController.setFrontViewPosition(.leftSideMost, animated: true)
        self.revealViewController().pushFrontViewController(nearbyLocationsMapViewController, animated: true)
            
          /*  if ((kUserDefaults.value(forKey: KRoleID)as? String) == kDealerID) {
                let storyBoard = UIStoryboard(name: kDealerDashboard, bundle: nil)
                let nearbyLocationsMapViewController = storyBoard.instantiateViewController(withIdentifier: kDealerIdentifier) as? UINavigationController
                
                revealController.setFront(nearbyLocationsMapViewController, animated: true)
                revealController.setFrontViewPosition(.leftSideMost, animated: true)
                
            self.revealViewController().pushFrontViewController(nearbyLocationsMapViewController, animated: true)
                
            } else if ((kUserDefaults.value(forKey: KRoleID)as? String) == kCustomerID) {
                let storyBoard = UIStoryboard(name: kCustomerDashboard, bundle: nil)
                let nearbyLocationsMapViewController = (storyBoard.instantiateViewController(withIdentifier: kCustomerIdentifier) as? UINavigationController)
                
                revealController.setFront(nearbyLocationsMapViewController, animated: true)
                revealController.setFrontViewPosition(.leftSideMost, animated: true)
                
                self.revealViewController().pushFrontViewController(nearbyLocationsMapViewController, animated: true)
            } else {
                let storyBoard = UIStoryboard(name: kServiceManDashboard, bundle: nil)
                let nearbyLocationsMapViewController = storyBoard.instantiateViewController(withIdentifier: kServiceManIdentifier) as? UINavigationController
                
                revealController.setFront(nearbyLocationsMapViewController, animated: true)
                revealController.setFrontViewPosition(.leftSideMost, animated: true)
                
                self.revealViewController().pushFrontViewController(nearbyLocationsMapViewController, animated: true)
            }*/
            
            break
        case 4:
            let storyBoard = UIStoryboard(name: kMainDashboard, bundle: nil)
            let nearbyLocationsMapViewController = storyBoard.instantiateViewController(withIdentifier: "HomeNavigationViewController") as? UINavigationController
            revealController.setFront(nearbyLocationsMapViewController, animated: true)
            revealController.setFrontViewPosition(.leftSideMost, animated: true)
            
            self.revealViewController().pushFrontViewController(nearbyLocationsMapViewController, animated: true)
          
        case 1:
            let storyBoard = UIStoryboard(name: kMainDashboard, bundle: nil)
            let nearbyLocationsMapViewController = storyBoard.instantiateViewController(withIdentifier: "Service") as? UINavigationController
            revealController.setFront(nearbyLocationsMapViewController, animated: true)
            revealController.setFrontViewPosition(.leftSideMost, animated: true)
            revealViewController().pushFrontViewController(nearbyLocationsMapViewController, animated: true)
            break
          
        case 2:
            let storyBoard = UIStoryboard(name: kMainDashboard, bundle: nil)
            let nearbyLocationsMapViewController = storyBoard.instantiateViewController(withIdentifier: "Near") as? NearbyLocationsMapViewController
            if let aController = nearbyLocationsMapViewController {
                present(aController, animated: true)
            }
            break
        case 3:
            signOutAction()
        default:
            break
        }
}
    func signOutAction() {
        controls.presentAlertController(withTitle: "Log Out", message: "Are you sure you want to logout?", delegate: self, block: { didOk in
            if didOk {
                kUserDefaults.set(false, forKey: "logged_in")
                appDelegate.pop(toViewController: "LoginNavigationController")
            }
        })
    }
    
    func configureMenuArray() {
        let dashboard = MenuObject()
        dashboard.imageName = "tractor"
        dashboard.title = "Dashboard"
        let vehicle = MenuObject()
        vehicle.imageName = "symbol"
        vehicle.title = "Vehicles"
        let service = MenuObject()
        service.imageName = "serviceblue"
        service.title = "Service"
        let obj1 = MenuObject()
        obj1.imageName = "petrolstation"
        obj1.title = "Nearest petrol stations"
        let obj = MenuObject()
        obj.imageName = "signoutblue"
        obj.title = "Sign Out"
        menuArray.add(dashboard)
       // menuArray.add(vehicle)
        menuArray.add(service)
        menuArray.add(obj1)
        menuArray.add(obj)
    }
}



//
//  NearbyLocationsMapViewController.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import CoreLocation
import GoogleMaps
import GooglePlaces
import UIKit

class NearbyLocationsMapViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    var mapView = GMSMapView()
    @IBOutlet var listView: UITableView!
    @IBOutlet var listButton: UIButton!
    
     var camera = GMSCameraPosition()
     var locationManager: CLLocationManager?
     var appDelegate: AppDelegate?
     var places = NSArray()
     var placeNames = NSArray()
     var location: CLLocation?
     var fontSize: CGFloat = 0.0
     var subfontSize: CGFloat = 0.0
     var isMapView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        view.backgroundColor = UIColor.white
        listView.rowHeight = UITableViewAutomaticDimension
        listView.estimatedRowHeight = 140
        if UI_USER_INTERFACE_IDIOM() == .pad {
            fontSize = 20.0
            subfontSize = 16.0
        } else {
            fontSize = 15.0
            subfontSize = 12.0
        }
        addGoogleMapView()
        listButton.isHidden = true
        isMapView = false
      
    }
    
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func showList(_ sender: UIButton) {
        if !isMapView {
            sender.setTitle("Mapview", for: .normal)
            UIView.transition(with: view, duration: 1.0, options: .transitionFlipFromRight, animations: {
                self.listView.isHidden = false
                self.isMapView = !self.isMapView
                DispatchQueue.main.async(execute: {
                    self.listView.reloadData()
                    self.mapView.isHidden = true
                })
            }) { finished in
            }
        } else {
            showPlacesonMap(places)
            sender.setTitle("Listview", for: .normal)
            UIView.transition(with: view, duration: 1.0, options: .transitionFlipFromLeft, animations: {
                self.mapView.isHidden = false
                self.isMapView = !self.isMapView
                DispatchQueue.main.async(execute: {
                    self.listView.isHidden = true
                })
            }) { finished in
            }
        }
    }
    
    func addGoogleMapView() {
        listView.isHidden = true
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 60, width: view.frame.size.width, height: view.frame.size.height - 60), camera: camera)
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.startUpdatingLocation()
        self.view.layoutIfNeeded()
        self.view.addSubview(mapView)
}
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        location = locations.last
        camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 12.0)
        mapView.animate(to: camera)
        manager.stopUpdatingLocation()
        manager.delegate = nil
        getNearByLocationsforLocation(location)
    }
    
    func getNearByLocationsforLocation(_ locat: CLLocation?) {
        var placesApi = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?keyword=petrol stations&key=\(KGoogleWebServiceApiKey)&location=\(locat?.coordinate.latitude ?? 0),\(locat?.coordinate.longitude ?? 0)&radius=20000"
        placesApi = placesApi.replacingOccurrences(of: " ", with: "%20")
        self.view.addActivity()
        let obj = NetworkManager(utility: networkUtility!)
        isfromPetrolstation = true
        obj.getNearsestPetrolStation(url: placesApi)
        {
            (response) in
            if response != nil
            {
                guard let status = ((response as? NSDictionary)?.value(forKey: "status") as? String) else
                {
                        self.view.removeActivity()
                    return
                }
                
                if status == "OK"
                {
                    guard let result = ((response as? NSDictionary)?.value(forKey: "results") as? NSArray) else
                    {
                        self.view.removeActivity()
                        return
                    }
                    if result.count > 0
                    {
                        self.placeNames = result
                        self.showPlacesonMap(self.placeNames)
                        self.listButton.isHidden = false
                    }
                    else
                    {
                        self.listButton.isHidden = true
                    }
                    DispatchQueue.main.async {
                        self.view.removeActivity()
                        self.listView.reloadData()
                    }
                }
            }
        }
    }
    func showPlacesonMap(_ locations: NSArray?) {
        var bounds = GMSCoordinateBounds()
        for locDict in locations!
        {
            let dict = locDict as? NSDictionary
            let sourceMarker = GMSMarker()
            sourceMarker.icon = UIImage(named: "petrolstation40")
            let latitude = CGFloat(Double((((dict?.value(forKey:"geometry") as? NSDictionary)?.value(forKey:"location") as? NSDictionary)?.value(forKey:"lat") as? Double ?? 0.0)!))
            let longitude = CGFloat(Double((((dict?.value(forKey:"geometry") as? NSDictionary)?.value(forKey:"location") as? NSDictionary)?.value(forKey:"lng") as? Double ?? 0.0)!))
            
            sourceMarker.position = CLLocationCoordinate2DMake(CLLocationDegrees(latitude), CLLocationDegrees(longitude))
            sourceMarker.title = dict?.value(forKey:"name") as? String
            sourceMarker.snippet = dict?.value(forKey:"vicinity") as? String
            bounds = bounds.includingCoordinate(sourceMarker.position)
            sourceMarker.appearAnimation = .pop
            sourceMarker.tracksViewChanges = false
            sourceMarker.map = mapView
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (placeNames.count > 0 ? placeNames.count:0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let addressDict = placeNames[indexPath.row] as? [AnyHashable : Any]
        let name = addressDict?["name"] as? String
        let address = addressDict?["vicinity"] as? String
        var attributedString = NSMutableAttributedString(string: "\(name ?? "")\n\(address ?? "")")
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: fontSize), range: NSRange(location: 0, length: name?.count ?? 0))
        attributedString.addAttribute(.foregroundColor, value: UIColor.gray, range: NSRange(location: (name?.count ?? 0) + 1, length: address?.count ?? 0))
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: subfontSize), range: NSRange(location: (name?.count ?? 0) + 1, length: address?.count ?? 0))

        let nameLbl = (cell?.viewWithTag(10) as! UILabel)
        nameLbl.attributedText = attributedString
       
        return cell!
}

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.selectedMarker = marker
        return true
    }
}

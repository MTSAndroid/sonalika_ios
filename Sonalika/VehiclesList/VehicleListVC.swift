//
//  VehicleListVC.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//

import Foundation
import UIKit

class VehiclesListVC: UIViewController,SWRevealViewControllerDelegate,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet var listView: UITableView!
    
     var vehicleListArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.setHidesBackButton(true, animated: true)
        title = "Vehicles"
        setslideViewController()
        listView.tableFooterView = UIView()
       // registerDeviceforPushNotifications()
    }
    
    func poproot(_ sender: Any?) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getVechiclesList()
    }
    
/*    func registerDeviceforPushNotifications() {
        let deviceID = UIDevice.current.identifierForVendor?.uuidString
        if UserDefaults.standard.object(forKey: "deviceToken") != nil {
            if let aKey = UserDefaults.standard.object(forKey: "deviceToken") as? String {
                if let aKey = UserDefaults.standard.object(forKey: "deviceToken") as? String {
                    HttpManager.sharedObject.postDataRequestWith(url: DeviceDataApi, method: kPost, parameters: ["deviceId": deviceID ?? 0, "pushToken": aKey, "platform": "IOS"], isLogin: false)
                    {
                        (response,error)in
                        if error == nil
                        {
                            print(response)
                        }
                    }
                }
            }
        }
    }*/
  
    func getVechiclesList() {
        var type = String()
        if (kUserDefaults.value(forKey: KRoleID) as? String == kDealerID) {
            type = "mynavi/dvmap/list?dealerID=\(kUserDefaults.value(forKey: KID)!)&limit=1000&page=1"
        } else if (kUserDefaults.value(forKey: KRoleID) as? String == kCustomerID) {
           type = "mynavi/dvmap/list?customerID=\(kUserDefaults.value(forKey: KID)!)&limit=1000&page=1"
        }
        else
        {
            type = "mynavi/dvmap/list?clientID=\(kUserDefaults.value(forKey: KID)!)&limit=1000&page=1"
        }
        
        APP_DELEGATE?.showActivityIndicatorOnFullScreen()
        let obj = NetworkManager(utility: networkUtility!)
        obj.getNearsestPetrolStation(url: type)
        {
            (response)in
            if response != nil
            {
                self.vehicleListArray.removeAllObjects()
               guard let rows = ((response as? NSDictionary)?.value(forKey: "rows") as? NSArray) else
               {
                APP_DELEGATE?.hideActivityIndcicator()
                return
               }
                for dict in rows
                {
                    let dict = dict as! NSDictionary
                    let obj = VehicleObject()
                    obj.vehicleName = self.checkNilString(dict.value(forKey:"vehicleName") as? String)!
                    obj.vehicleNumber = self.checkNilString(dict.value(forKey: "vehicleNumber") as? String)!
                    obj.vehicleType = self.checkNilString(dict.value(forKey:"vehicleType") as? String)!
                    if self.checkKeyExists(dict: dict, key: "lat")
                    {
                       obj.latitude = Double((self.checkNilString(dict.value(forKey: "lat")as? String))!)!
                    }
                    else
                    {
                        obj.latitude = 0.0
                    }
                    if self.checkKeyExists(dict: dict, key: "lng")
                    {
                        obj.longitude = Double((self.checkNilString(dict.value(forKey: "lng")as? String))!)!
                    }
                    else
                    {
                        obj.longitude = 0.0
                    }
                  
                    //details Array
                    obj.deviceModel = self.checkNilString(dict.value(forKey:"deviceModel")as? String)!
                    obj.deviceID = self.checkNilString(dict.value(forKey:"deviceID") as? String)!
                    obj.engineLastOn = self.checkNilString(dict.value(forKey: "lastEngineOn") as? String)!
                    if !(obj.engineLastOn == "NA") && !(obj.engineLastOn == "") {
                        obj.engineLastOn = self.getDateString(obj.engineLastOn)!
                    }
                    obj.totalEngineHours = self.checkNilString(dict.value(forKey:"totalEngineHours") as? String)!
                    obj.vehicleLocation = (self.checkNilString(dict.value(forKey:"vehicleLocation") as? String)!)
                    let dateString = (self.checkNilString(dict.value(forKey:"lastDateReceivedAt") as? String))
                    if !(dateString == "") && !(dateString == "NA") {
                        let actualDate = CustomControls.sharedObj.addFiveHoursthirtymins(dateString)
                        obj.lastDataReceivedAt = self.getDateString(actualDate)!
                    } else {
                        obj.lastDataReceivedAt = "NA"
                    }
                    self.vehicleListArray.add(obj)
                
                }
                DispatchQueue.main.async {
                    self.listView.reloadData()
                }
            }
            OperationQueue.main.addOperation {
                APP_DELEGATE?.hideActivityIndcicator()
            }
        }
    }
    func setslideViewController() {
        let revealController: SWRevealViewController? = revealViewController()
        revealController?.delegate = self
        revealController?.rightViewController = nil
        if let aRecognizer = revealController?.panGestureRecognizer {
            view.addGestureRecognizer(aRecognizer())
        }
        let revealButtonItem = UIBarButtonItem(image: UIImage(named: "menu_icon"), style: .plain, target: revealController, action: #selector(revealController?.revealToggle(_:)))
        navigationItem.leftBarButtonItem = revealButtonItem
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (vehicleListArray.count>0 ? vehicleListArray.count:0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? HomeCell
        let obj = vehicleListArray[indexPath.row] as? VehicleObject
        cell?.vehicleNumberlable.text = obj?.vehicleNumber.uppercased()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let tabViewControllerObj = storyBoard.instantiateViewController(withIdentifier: "TabViewController") as? TabViewController
        tabViewControllerObj?.vehicleObject = vehicleListArray[indexPath.row] as? VehicleObject
        self.navigationController?.pushViewController(tabViewControllerObj!, animated: true)
    }
    func getDateString(_ sender: String?) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let date: Date? = dateFormatter.date(from: sender ?? "")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        var convertedDateString: String? = nil
        if let aDate = date {
            convertedDateString = dateFormatter.string(from: aDate)
        }
        return convertedDateString
    }
    
    func checkNilString(_ val: String?) -> String? {
        if val != nil {
            return val
        } else {
            return "NA"
        }
    }
    
    func checkKeyExists(dict:NSDictionary,key:String) -> Bool
    {
        if dict.value(forKey: key) != nil
        {
            return true
        }
        else
        { return false
        }
    }
}

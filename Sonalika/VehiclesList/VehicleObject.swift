//
//  VehicleObject.swift
//  Ajax fiori Swift
//
//  Created by Manikumar on 02/08/18.
//  Copyright © 2018 Trakitnow. All rights reserved.
//


import Foundation

class VehicleObject: NSObject {
    var vehicleName = ""
    var vehicleNumber = ""
    var vehicleType = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var deviceModel = ""
    var deviceID = ""
    var engineLastOn = ""
    var totalEngineHours = ""
    var lastDataReceivedAt = ""
    var vehicleLocation = ""
}
